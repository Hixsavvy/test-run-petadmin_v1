import React, { Fragment, Component } from 'react';
import Navbar from '../components/navbar/Navbar';

class Categories extends Component {
  render () {
    return (
      <div className="categories">
        <div className="container">
          <Navbar />
        </div>
        {this.props.children}
      </div>
    )
  }
}

export default Categories;