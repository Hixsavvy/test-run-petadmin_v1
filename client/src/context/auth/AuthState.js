import React, { useReducer } from 'react';
import axios from 'axios';
import AuthContext from './authContext'; 
import authReducer from './authReducer'; 
import setAuthToken from '../../utils/setAuthToken'; 
import {
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  USER_LOADED,
  SET_CURRENT_USER,
  UPDATE_USER,
  CLEAR_CURRENT_USER,
  CLEAR_USER,
  AUTH_ERROR,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  UPDATE_FAIL,
  LOGOUT,
  CLEAR_ERRORS
} from '../types'; 

const AuthState = props => {
  const initialState = {
    token: localStorage.getItem('token'),
    isAuthenticated: null,
    user: null,
    loading: true,
    error: null
  };

  const [state, dispatch] = useReducer(authReducer, initialState);

  // Load User
  const loadUser = async () => {
    // To get token from local storage
    if (localStorage.token) {
      setAuthToken(localStorage.token);
    }

    try {
      const res = await axios.get('api/v1/auth/get-current-user');

      dispatch({
        type: USER_LOADED,
        payload: res.data
      });
      
    } catch (err) {
      dispatch({ type: AUTH_ERROR });
    }
  };
  
  
  // Register User 
  const register = async formData => {
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }

  try {
    const res = await axios.post('/api/v1/users/register', formData, config); 

    dispatch({
      type: REGISTER_SUCCESS,
      payload: res.data
    });

    // After action dispatched, then, load user
    loadUser();
    
  } catch (err) {
    dispatch({
      type: REGISTER_FAIL,
      payload: err.response.data.msg
    });
  }
};

  // Login User
  const login = async formData => {
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }

  try {
    const res = await axios.post('/api/v1/auth', formData, config); 

    dispatch({
      type: LOGIN_SUCCESS,
      payload: res.data
    });

    // After action dispatched, then, load user
    loadUser();
    
  } catch (err) {
    dispatch({
      type: LOGIN_FAIL,
      payload: err.response.data.msg
    });
  }
};


  // Update User
  const updateUser = async userData => {
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }

  try {
    const res = await axios.post('/api/v1/users/update', userData, config); 

    dispatch({
      type: UPDATE_USER,
      payload: res.data
    });

    // After action dispatched, then, load user
    loadUser();
    
  } catch (err) {
    dispatch({
      type: UPDATE_FAIL,
      payload: err.response.data.msg
    });
    }
  };

  

  // Set Current User Data
  const setUser = user => {
    dispatch({ 
      type: SET_CURRENT_USER, 
      payload: user 
    });
  };


  // Logout
  const logout = () => 
    dispatch({ 
      type: LOGOUT 
  });


  // Clear Current User
  const clearCurrentUser = () => {
    dispatch({ 
      type: CLEAR_CURRENT_USER
    });
  };


  // Clear Single User
  const clearUser = () => {
    dispatch({
      type: CLEAR_USER
    });
  };


  // Clear Errors
  const clearErrors = () => 
    dispatch({ 
      type: CLEAR_ERRORS 
    });

   

  return (
    <AuthContext.Provider
      value={{
        token: state.token,
        user: state.user,
        //currentUser: state.currentUser,
        isAuthenticated: state.isAuthenticated,
        loading: state.loading,
        error: state.error,
        register,
        loadUser,
        login,
        setUser,
        updateUser,
        clearUser,
        clearCurrentUser,
        logout,
        clearErrors
      }}
    >
      {props.children}
    </AuthContext.Provider>
  );
};

export default AuthState; 