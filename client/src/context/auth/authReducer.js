import {
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  USER_LOADED,
  SET_CURRENT_USER,
  CLEAR_CURRENT_USER,
  UPDATE_USER,
  CLEAR_USER,
  UPDATE_FAIL,
  AUTH_ERROR,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT,
  CLEAR_ERRORS
} from '../types';

export default (state, action) => {
  switch(action.type) {
    case USER_LOADED:
    case SET_CURRENT_USER:
      return {
        ...state,
        isAuthenticated: true,
        loading: false,
        user: action.payload
      };
      case UPDATE_USER:
      return {
        ...state,
        users: state.users.map(user => 
          user._id === action.payload.id ? action.payload : user
        ),
        loading: false
      };
    case CLEAR_CURRENT_USER:
      return {
        ...state,
        user: null
      };
    case CLEAR_USER:
      return {
        ...state,
        user: null,
        loading: null,
        filtered: null,
        error: null
      }; 
    case REGISTER_SUCCESS:
    case LOGIN_SUCCESS:
      // Add token to local storage
      localStorage.setItem('token', action.payload.token);
      return {
        ...state,
        ...action.payload,
        isAuthenticated: true,
        loading: false
      }; 
    case REGISTER_FAIL:
    case AUTH_ERROR:
    case LOGIN_FAIL:
    case UPDATE_FAIL:
    case LOGOUT:
      localStorage.removeItem('token');
      return {
        ...state,
        token: null,
        isAuthenticated: false,
        loading: false,
        user: null,
        error: action.payload
      };
    case CLEAR_ERRORS:
      return {
        ...state,
        error: null
      };
    default:
      return state
  }
}  