import {
  ADD_PET,
  GET_PET,
  SET_CURRENT_PET,
  CLEAR_CURRENT_PET,
  GET_PETBIO,
  ADDPET_TO_OWNBIO,
  PET_ERROR,
  PETBIO_ERROR,
  DELETE_PET,
  CLEAR_PET,
  CLEAR_PETBIO,
  CLEAR_PETBIOS,
  UPDATE_PETBIO,
  GET_PETBIOS,
  FILTER_PETBIOS
} from '../types'; 

export default (state, action) => {
  switch(action.type) {
    case GET_PET:
      return {
        ...state,
        // pet: action.payload.id,
        pet: action.payload,
        loading: false
      };
    case GET_PETBIO:
      return {
        ...state,
        //pet: action.payload,
        pet: action.payload.petbio,
        loading: false
      };
    case ADD_PET:
    case ADDPET_TO_OWNBIO:
      return {
        ...state,
        pet: action.payload,
        loading: false
      };
    case SET_CURRENT_PET:
      return {
        ...state,
        pet: action.payload
        //currentPet: action.payload
      };
    case CLEAR_CURRENT_PET:
      return {
        ...state,
        currentPet: null
      }; 
    case DELETE_PET:
      return {
        ...state,
        pets: state.pets.map(pet => 
          pet._id !== action.payload.id
        ),
        //ownbio: state.ownbio.splice(ownbio.listOf('pet'), 1),
        loading: false
      }; 
    case CLEAR_PET:
      return {
        ...state,
        pet: null,
        error: null,
        filtered: null,
        currentPet: null
      }; 
    case PET_ERROR:
    case PETBIO_ERROR:
      return {
        ...state,
        error: action.payload,
        loading: false
      };
    
  }
}