import React, { useReducer } from 'react';
import axios from 'axios';
import PetContext from './petContext';
import petReducer from './petReducer';

import {
  ADD_PET,
  GET_PET,
  SET_CURRENT_PET,
  GET_PETBIO,
  ADDPET_TO_OWNBIO,
  PET_ERROR,
  PETBIO_ERROR,
  DELETE_PET,
  CLEAR_PET,
  CLEAR_CURRENT_PET,
  CLEAR_PETBIO,
  CLEAR_PETBIOS,
  UPDATE_PETBIO,
  GET_PETBIOS,
  FILTER_PETBIOS
} from '../types'; 

const PetState = props => {
  const initialState = {
    pet: '',
    pets: [],
    currentPet: null,
    filtered: null,
    error: null
  };

  const [state, dispatch] = useReducer(petReducer, initialState);

  // Get a single Pet
  const getPet = async () => {
    try {
      const res = await axios.get('api/v10/pets/get-pet-doc'); 

      dispatch({
        type: GET_PET,
        payload: res.data
      });

      // After action dispatched, makeget the pet data available
      //setCurrentPet();
      
    } catch (err) {
      dispatch({ type: PET_ERROR });
    }
  };



  // Add pet
  const addPet = async pet => {
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }

    try {
      const res = await axios.post('api/v10/pets/add-a-pet', pet, config); 

      dispatch({ type: ADD_PET, payload: res.data }); 
      
      // After action dispatched, add pet to ownbio 
      //getPet();
      setCurrentPet();

    } catch (err) {
      dispatch({ 
        type: PET_ERROR
        //,
        //payload: err.response.msg @TODO 
        // NEEDS ERROR RESPONSE TO BE RETURNED - MISSING IN CORRESPONDING ownbio.js route???
        // or TRY err.response.data.msg?? 
      });
    }
  };
  
  
  // Get All Petbio data of a given owner's pet status
  // - it could be (one) pet doc having array of multiple petbios 
  const getPetBio = async () => {
    try {
      const res = await axios.get('api/v10/pets/get-petbio'); 

      dispatch({ type: GET_PETBIO, payload: res.data }); 

    } catch (err) {
      dispatch({ 
        type: PETBIO_ERROR
        //,
        //payload: err.response.msg
        //FIX THIS ERRORS BUG LATER!! - TRY err.response.data.msg?? 
      });
    }
  };


  // App a Pet to Owner Bio 
  const addPetOwnBio = async pet => {
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }

    try {
      //const res = await axios.put('api/v10/pets/put-link-pet-to-ownbio', pet, config);
      // const res = await axios.post('api/v10/pets/link-pet-to-ownbio', pet, config); 
      const res = await axios.post('api/v10/pets/link-pet-2-ownbio', pet, config);

      dispatch({ type: ADDPET_TO_OWNBIO, payload: res.data }); 
      
    } catch (err) {
      dispatch({ 
        type: PETBIO_ERROR
        //,
        //payload: err.response.msg @TODO 
        // NEEDS ERROR RESPONSE TO BE RETURNED - MISSING IN CORRESPONDING ownbio.js route???
        // or TRY err.response.data.msg?? 
      });
    }
  };
  
  
  // Clear a pet
  const clearPet = () => {
    dispatch({
      type: CLEAR_PET
    });
  };

  // Set Current Pet
  const setCurrentPet = pet => {
    dispatch({ 
      type: SET_CURRENT_PET, 
      payload: pet 
    });

    // After action dispatched, add the pet to ownbio 
    getPet();
  };



  // Clear Current Pet CLEAR_CURRENT_PET
  const clearCurrentPet = () => {
    dispatch({
      type: CLEAR_CURRENT_PET
    });
  };



  // Update Pet



  // Delete Pet
  const deletePet = async id => {
    try {
      await axios.delete('api/v10/pets/delete');

      dispatch({ 
        type: DELETE_PET, 
        payload: id 
      });
      
    } catch (err) {
      dispatch({ 
        type: PET_ERROR
        //,
        //payload: err.response.msg
        //FIX THIS ERRORS BUG LATER!! - TRY err.response.data.msg??
      }); 
    }
  }; 


  



  // Clear petbio CLEAR_PETBIO

  


  // Get all pets


  return (
    <PetContext.Provider 
      value={{
        pet: state.pet,
        pets: state.pets,
        currentPet: state.currentPet,
        filtered: state.filtered,
        error: state.error,
        getPet,
        addPet,
        addPetOwnBio,
        getPetBio,
        setCurrentPet,
        clearCurrentPet,
        deletePet,
        clearPet
      }}
    >
      {props.children}
    </PetContext.Provider>
  )
}

export default PetState
