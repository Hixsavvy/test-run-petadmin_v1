import {
  GET_OWNBIO,
  ADD_OWNBIO,
  GET_A_PET,
  OWNBIO_ERROR,
  DELETE_OWNBIO,
  OWNBIO_DEL_PET_OBJECT,
  CLEAR_OWNBIO,
  CLEAR_OWNBIOS,
  SET_CURRENT,
  CLEAR_CURRENT,
  UPDATE_OWNBIO,
  GET_OWNBIOS,
  ADDRESS_TO_OWNBIO,
  FILTER_OWNBIOS,
  CLEAR_FILTER
} from '../types'; 

export default (state, action) => {
  switch(action.type) {
    case GET_OWNBIO:
      return {
        ...state,
        ownbio: action.payload,
        loading: false
      };
      case GET_A_PET:
        return {
          ...state,
          //ownbio: action.payload.pet,
          pet: action.payload,
          //ownbio: action.payload, 
          loading: false
        };
    case ADD_OWNBIO:
    case ADDRESS_TO_OWNBIO:
      return {
        ...state,
        ownbio: action.payload,
        loading: false
      }; 
    case UPDATE_OWNBIO:
      return {
        ...state,
        ownbios: state.ownbios.map(ownbio => 
          ownbio.id === action.payload.id ? action.payload : ownbio
        ),
        loading: false
      };
    case DELETE_OWNBIO:
      return {
        ...state,
        ownbios: state.ownbios.map(ownbio => 
          ownbio._id !== action.payload.id
        ),
        loading: false
      }; 
    // Filter probably not most ideal for clearing
    // pet object from state after delete in of same
    // in pet collection - splice might be a better fit
    case OWNBIO_DEL_PET_OBJECT:
      return {
        ...state,
        // ownbio: state.ownbio.filter(ownbio => {
        //   //return ownbio !== 'pet'
        //   return action.payload.ownbio !== 'pet'
        // }),
        ownbio: state.splice(ownbio => (ownbio.indexOf('pet'), 1) === action.payload 
        ), 
        // ownbio: state.splice(ownbio => {
        //   return (ownbio.indexOf('pet'), 1) == action.payload 
        // }),
        //ownbio: state.ownbio.splice(ownbio.indexOf('pet'), 1),
        loading: false
        //current: ownbio
      };
    case CLEAR_OWNBIO:
      return {
        ...state,
        ownbio: null,
        filtered: null,
        error: null,
        current: null
      };
    case CLEAR_OWNBIOS:
      return {
        ...state,
        ownbios: null,
        filtered: null,
        error: null,
        current: null
      };
    case SET_CURRENT:
      return {
        ...state,
        current: action.payload
      };
    case CLEAR_CURRENT:
      return {
        ...state,
        current: null
      };
    case GET_OWNBIOS:
      return {
        ...state,
        ownbios: action.payload,
        loading: false
      };
    case FILTER_OWNBIOS:
      return {
        ...state,
        filtered: state.ownbio.filter(ownbio => {
          const regex = new RegExp(`${action.payload}`, 'gi');
          return ownbio.vetname.match(regex); 
        })
      };  
    case CLEAR_FILTER:
      return {
        ...state,
        filtered: null
      };
    case OWNBIO_ERROR:
      return {
        ...state,
        error: action.payload,
        loading: false
      }; 
    default:
      return state;
  }
}; 