import { createContext } from 'react';

const ownbioContext = createContext();

export default ownbioContext;