import React, { Fragment, useReducer } from 'react';
import axios from 'axios';
import OwnBioContext from './ownbioContext'; 
import ownbioReducer from './ownbioReducer';
// import PetContext from '../../context/pet/petContext';
import {
  GET_OWNBIO,
  GET_A_PET,
  ADD_OWNBIO,
  OWNBIO_ERROR,
  DELETE_OWNBIO,
  CLEAR_OWNBIO,
  OWNBIO_DEL_PET_OBJECT,
  SET_CURRENT,
  CLEAR_CURRENT,
  CLEAR_OWNBIOS,
  UPDATE_OWNBIO,
  GET_OWNBIOS,
  ADDRESS_TO_OWNBIO,
  FILTER_OWNBIOS,
  CLEAR_FILTER
} from '../types'; 


const OwnBioState = props => {
  const initialState = {
    ownbio: '', 
    ownbios: [],
    current: null,
    filtered: null,
    error: null
  }; 

  const [state, dispatch] = useReducer(ownbioReducer, initialState);
 

  // Get an Owner Bio
  const getOwnBio = async () => {
    try {
      const res = await axios.get('api/v10/ownbio/named');
      //const res = await axios.get('api/v10/ownbio/owners-pet');

      dispatch({ type: GET_OWNBIO, payload: res.data }); 
      
      // After action dispatched, make the pet data available
      getPetData();

    } catch (err) {
      dispatch({ 
        type: OWNBIO_ERROR
        //,
        //payload: err.response.msg
        //FIX THIS ERRORS BUG LATER!! - TRY err.response.data.msg??
      });
    }
  };

  // Get pet details 
  const getPetData = async () => {
    try {
      //const res = await axios.get('api/v10/ownbio/owner-pet'); 
      const res = await axios.get('api/v10/ownbio/owners-pet');

      dispatch({ type: GET_A_PET, payload: res.data }); 

    } catch (err) {
      dispatch({ 
        type: OWNBIO_ERROR
        //,
        //payload: err.response.msg
        //FIX THIS ERRORS BUG LATER!! - TRY err.response.data.msg??  
      });
    }
  };
  

  // Add Owner Bio
  const addOwnBio = async ownbio => {
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }

    try {
      const res = await axios.post('api/v10/ownbio/link-ownbio-to-user', ownbio, config); 

      dispatch({ type: ADD_OWNBIO, payload: res.data }); 
      
      // After action dispatched, add pet to ownbio
      // addPetOwnBio();

    } catch (err) {
      dispatch({ 
        type: OWNBIO_ERROR
        //,
        //payload: err.response.msg @TODO 
        // NEEDS ERROR RESPONSE TO BE RETURNED - MISSING IN CORRESPONDING ownbio.js route???
        // or TRY err.response.data.msg?? 
      });
    }
  };

 
  // Delete Owner Bio 
  const deleteOwnBio = async id => {
    try {
      await axios.delete('api/v10/ownbio/delete');

      dispatch({ 
        type: DELETE_OWNBIO, 
        payload: id 
      });
      
    } catch (err) {
      dispatch({ 
        type: OWNBIO_ERROR
        //,
        //payload: err.response.msg
        //FIX THIS ERRORS BUG LATER!! - TRY err.response.data.msg??
      }); 
    }
  };


  // Delete pet, and remove pet object from owners bio 
  const delPetRemFrmOwnBio = async ownbio => {
    try {
      await axios.delete('api/v10/ownbio/delete-pet-pet');

      dispatch({ 
        type: OWNBIO_DEL_PET_OBJECT, 
       // payload: ownbio 
      });
      
    } catch (err) {
      dispatch({ 
        type: OWNBIO_ERROR
        //,
        //payload: err.response.msg
        //FIX THIS ERRORS BUG LATER!! - TRY err.response.data.msg??
      }); 
    }
  };



// Update Owner Bio 
  const updateOwnBio = async ownbio => {
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }

    try {
      const res = await axios.put('api/v10/ownbio/update', ownbio, config);

      dispatch({ 
        type: UPDATE_OWNBIO, 
        payload: res.data 
      });

    } catch (err) {
      dispatch({ 
        type: OWNBIO_ERROR,
        payload: err.response.msg
      });
    }
  }; 


  
  // Add address to owner bio
  const addressToOwnBio = async ownbio => {
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }

    try {
      const res = await axios.post('api/v10/ownbio/address-to-bio', ownbio, config); 

      dispatch({ type: ADDRESS_TO_OWNBIO, payload: res.data }); 
      
    } catch (err) {
      dispatch({ 
        type: OWNBIO_ERROR,
        payload: err.response.data.msg
        //,
        //payload: err.response.msg @TODO 
        // NEEDS ERROR RESPONSE TO BE RETURNED - MISSING IN CORRESPONDING ownbio.js route???
        // or TRY err.response.data.msg??
      });
    }
  }; 



  // Get ALL Owner Bios 
  const getOwnBios = async () => {
    try {
      const res = await axios.get('api/v10/ownbio/all'); 

      dispatch({ type: GET_OWNBIOS, payload: res.data }); 

    } catch (err) {
      dispatch({ 
        type: OWNBIO_ERROR,
        payload: err.response.msg
      });
    }
  };


  // Clear Single Owner Bio 
  const clearOwnBio = () => {
    dispatch({ 
      type: CLEAR_OWNBIO 
    });
  };


  // Clear ALL Owner Bios
  const clearOwnBios = () => {
    dispatch({ type: CLEAR_OWNBIOS });
  };
  
    
  // Set Current Owner Bio
  const setCurrent = ownbio => {
    dispatch({ type: SET_CURRENT, payload: ownbio });
  };

  // Clear Current Owner Bio
  const clearCurrent = () => {
    dispatch({ 
      type: CLEAR_CURRENT 
    });
  };

  
  // Filter Owner Bios 
  const filterOwnBios = text => {
    dispatch({ 
      type: FILTER_OWNBIOS, 
      payload: text 
    });
  };


  // Clear Filter
  const clearFilter = () => {
    dispatch({ 
      type: CLEAR_FILTER 
    }); 
  }; 
   

  return (
    <Fragment>
      <OwnBioContext.Provider
        value={{
          ownbio: state.ownbio,
          ownbios: state.ownbios,
          current: state.current,
          filtered: state.filtered,
          error: state.error,
          getOwnBio,
          addOwnBio,
          deleteOwnBio,
          delPetRemFrmOwnBio,
          addressToOwnBio,
          setCurrent,
          getPetData,
          clearCurrent,
          updateOwnBio,
          filterOwnBios,
          clearFilter,
          getOwnBios,
          clearOwnBio,
          clearOwnBios
        }}
      >
        {props.children}
      </OwnBioContext.Provider>
    </Fragment>
  );
}; 

export default OwnBioState;