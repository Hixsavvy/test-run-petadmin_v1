import React, { Fragment, useState } from 'react';
import axios from 'axios'; 

const SupAdminAddUser = () => {
  const [formData, setFormData] = useState({
    id: 'id',
    firstname: '',
    lastname: '',
    contactnumber: '',
    email: '',
    password: '',
    password2: '',
    vetname: '',
    specialneeds: '',
    age: ''
  });

  const { id, firstname, lastname, contactnumber, email, password, password2, vetname, specialneeds, age } = formData;

  const onChange = e => setFormData({ ...formData,  [e.target.name]: e.target.value }); 
 
  const onSubmit = async e => {
    e.preventDefault();
    if(password !== password2) {
      console.log('Passwords do not match');
      return;
    } else {
      const newUser = {
        id,
        firstname,
        lastname,
        contactnumber,
        email,
        password,

        ownbio: {
          vetname,
          specialneeds
        },

        pets: { age }
      }

      try {
        // Since we are sending data, create 
        // config object with headers object 
        const config = {
          headers: {
            'Content-Type': 'application/json'
          }
        }

        const body = JSON.stringify(newUser);
        
        const res = await 
          axios.post('/api/v1/users/register', body, config);
          console.log(res.data);

        let ownbio = await
          axios.post('/add-owner-bio', ownbio);

        let pets = await
          axios.post('/addpet-to-ownbio');

      } catch(err) {
        console.error(err.response.data); 
      }

      setFormData({
        firstname: '',
        lastname: '',
        contactnumber: '',
        email: '',
        password: '',
        password2: '',
        vetname: '',
        specialneeds: '',
        age: ''
      });
    }
  };

  return (
    <Fragment>
      <div className="register-user">
        <div className="form-container">
          <div className="w3-row-padding">
            <div className="w3-half w3-margin-bottom">
              <form className="w3-container w3-card-4" onSubmit={e => onSubmit(e)}>
                <h3>Register new client here</h3>
                <p>Please enter user details.</p>

                <div className="w3-section">
                  <input 
                      className="w3-input" 
                      id="addFirstNameInput"
                      type="text" required
                      name="firstname" 
                      placeholder="Please enter users first name here"
                      value={firstname}
                      onChange={e => onChange(e)}
                    />
                    <label>First name:</label>
                </div>

                <div className="w3-section">
                  <input 
                      className="w3-input" 
                      id="addLastNameInput"
                      type="text" required
                      name="lastname" 
                      placeholder="Please enter users last name here"
                      value={lastname}
                      onChange={e => onChange(e)}
                    />
                    <label>Last name:</label>
                </div>

                <div className="w3-section">
                  <input 
                    className="w3-input" 
                    id="addContactnumberInput"
                    type="number" required
                    name="contactnumber" 
                    placeholder="Please enter users phone number here"
                    value={contactnumber}
                    onChange={e => onChange(e)}
                  />
                  <label>Phone:</label>
                </div>

                <div className="w3-section">
                  <input 
                    className="w3-input" 
                    id="addEmailInput"
                    type="email"
                    name="email" 
                    placeholder="Please enter users email here"
                    value={email}
                    onChange={e => onChange(e)}
                  />
                  <label>Email:</label>
                </div>

                <div className="w3-section">
                  <input 
                      className="w3-input" 
                      id="addPasswordInput"
                      type="password" required
                      name="password" 
                      placeholder="Please enter users password here"
                      value={password}
                      onChange={e => onChange(e)}
                    />
                    <label>Password:</label>
                </div>

                <div className="w3-section">
                  <input 
                      className="w3-input" 
                      id="addConfirmPasswordInput"
                      type="password" required
                      name="password2" 
                      placeholder="Please confirm password here"
                      value={password2}
                      onChange={e => onChange(e)}
                    />
                    <label>Confirm password:</label>
                </div>

                <div className="w3-section">
                  <input 
                      className="w3-input" 
                      id="addVetNameInput"
                      type="text" required
                      name="vetname" 
                      placeholder="Please enter vets name here"
                      value={vetname}
                      onChange={e => onChange(e)}
                    />
                    <label>Vet name:</label>
                </div>

                <div className="w3-section">
                  <input 
                      className="w3-input" 
                      id="addSpecialneedsInput"
                      type="textarea"
                      name="specialneeds" 
                      placeholder="Please enter any special needs for your pet"
                      value={specialneeds}
                      onChange={e => onChange(e)}
                    />
                    <label>Special needs:</label>
                </div>

                <div className="w3-section">
                  <input 
                      className="w3-input" 
                      id="addAgeInput"
                      type="text" required
                      name="age" 
                      placeholder="Please enter pes age here"
                      value={age}
                      onChange={e => onChange(e)}
                    />
                    <label>Pets age:</label>
                </div>

                <div className="w3-section">
                  <p>
                  <button 
                    className="w3-button w3-block w3-black w3-margin-bottom"
                    id="SupAdminRegUserButton"
                    type="submit">
                      Register
                  </button>
                  </p>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default SupAdminAddUser;