import React, { Component } from 'react'; 
 
import axios from 'axios';
import setAuthToken from '../../../utils/setAuthToken';

import PetRegClient2 from '../../ownbios/pets/PetRegClient2';
import OwnRegClient from '../../ownbios/owners/OwnRegClient';
import AddressRegClient from '../../common/elements/AddressRegClient';
import SuccessRegClient from '../../common/elements/SuccessRegClient';

class RegUserClientForm extends Component {
  constructor() {
    super();
    this.state = {
      step: 1,

      firstname: '',
      lastname: '',
      contactnumber: '',
      email: '',
      password: '',
      password2: '',

      vetname: '',
      specialneeds: '',

      //pets: {},

      age: '',
      avatar: '',

      petname: '',
      pettype: '',
      petbreed: '',

      address_house: '',
      address_street: '',
      address_street2: '',
      address_post_code: '',
      address_city: '',
            
      errors: {}
    };
 
    this.nextStep = this.nextStep.bind(this); 
    this.prevStep = this.prevStep.bind(this);
  }

  prevStep = async e => {
    const { step } = this.state
    this.setState({
      step: step - 1
    })
  }

  nextStep = async e => {
    const { step } = this.state;
    if (step !==3) {
      this.setState({ step: step + 1 });
    } else {
      alert("Submitting! Name: ${firstname}{' '} {lastname}  Phone: ${contactnumber}");
      
      const newUser = {
        id: this.state.id,
        token: this.state.token,
        firstname: this.state.firstname,
        lastname: this.state.lastname,
        contactnumber: this.state.contactnumber,
        email: this.state.email,
        password: this.state.password,

        address: {
          house: this.state.address_house,
          street: this.state.address_street,
          street2: this.state.address_street2,
          post_code: this.state.address_post_code,
          city: this.state.address_city,
        },

        ownbio: {
          // vetname: this.state.vetname,
          // specialneeds: this.state.specialneeds,
          contactnumber: this.state.contactnumber
        },
        
        pets: {
          petname: this.state.petname,
          pettype: this.state.pettype,
          petbreed: this.state.petbreed,
          age: this.state.age,
          avatar: this.state.avatar
        },

        errors: {}
    };

    try {
        //Since we are sending data, create 
        //config object with headers object 
        const config = {
          headers: {
            'Content-Type': 'application/json',
           // 'x-auth-token': 'token'
          }
        }

        const body = JSON.stringify(newUser);

        const res = await 
          axios.post('/api/v1/users/register', body, config);
          console.log(res.data);
          
          // let token = res.data;

          // if (token) {
          //   setAuthToken(token)
          // }
        
        // Load User

        // let user = await
        //   axios.get('/api/get-user');

        // const payload = {
        //   user: {
        //     id: user.id
        //   }
        // };

      
        let ownbio; 
          await axios.post('/api/v10/ownbio/add-ownbio-to-user', ownbio); 
          
        //   await axios.post('/api/v10/ownbio/addpet-to-ownbio', ownbio.pet);

        //   //let pets = 
        //   await axios.post('/api/v10/pets/add-petbio', ownbio.pets);
        //console.log(ownbio);
        
      } catch (err) {
        console.error(err.response.data);
      }
    }
  }

    onChange(field) {
      return (e) => this.setState({ [field]: e.target.value });
    }

    render() {
      switch(this.state.step) {
        case 1:
          return (
            <OwnRegClient 
              key="owner"
              onSubmit={this.nextStep}
              firstname={this.state.firstname}
              lastname={this.state.lastname}
              contactnumber={this.state.contactnumber}
              email={this.state.email}
              password={this.state.password}
              password2={this.state.password2}
              vetname={this.state.vetname}
              onFirstNameChange={this.onChange("firstname")}
              onLastNameChange={this.onChange("lastname")}
              onContactNumberChange={this.onChange("contactnumber")}
              onEmailChange={this.onChange("email")}
              onPasswordChange={this.onChange("password")}
              onPassword2Change={this.onChange("password2")}
              onVetnameChange={this.onChange("vetname")}
            />
            
          );
        case 2:
          return (
            <PetRegClient2 
              key="pets"
              onSubmit={this.nextStep}
              petname={this.state.petname}
              age={this.state.age}
              pettype={this.state.pettype}
              specialneeds={this.state.specialneeds}
              petbreed={this.state.petbreed}
              onPetNameChange={this.onChange("petname")}
              onAgeChange={this.onChange("age")}
              onPetTypeChange={this.onChange("pettype")}
              onSpecialNeedsChange={this.onChange("specialneeds")}
              onPetBreedChange={this.onChange("petbreed")}
            />
          );
        case 3: 
          return (
            <AddressRegClient
              key="address"
              onSubmit={this.nextStep}
              //onClick={this.resetForm} 
              addressHouse={this.state.address_house}
              addressStreet={this.state.address_street}
              addressStreet2={this.state.address_street2}
              addressPostCode={this.state.address_post_code}
              addressCity={this.state.address_city}
              onAddressHouseChange={this.onChange("address_house")}
              onAddressStreetChange={this.onChange("address_street")}
              onAddressStreet2Change={this.onChange("address_street2")}
              onAddressPostCodeChange={this.onChange("address_post_code")}
              onAddressCityChange={this.onChange("address_city")}
            /> 
          );
        case 4: 
          return (
            <SuccessRegClient
              key="success"
            />
          );
      //default:
    }
  }
}
 
export default RegUserClientForm;