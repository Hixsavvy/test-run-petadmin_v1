import React, { Fragment, useState, useContext, useEffect } from 'react';
import AuthContext from '../../../../context/auth/authContext';
import AlertContext from '../../../../context/alert/alertContext';

const LoginUserClient = (props) => {
  const authContext = useContext(AuthContext);
  const alertContext = useContext(AlertContext);

  const { login, error, clearErrors, isAuthenticated } = authContext;
  const { setAlert } = alertContext;

  useEffect(() => {
    if (isAuthenticated) {
      props.history.push('/welcome');
    }

    if (error === 'Invalid user details!') {
      setAlert(error, 'danger');
      clearErrors();
    }
    // eslint-disable-next-line
  }, [error, isAuthenticated, props.history]);

  const [user, setUser] = useState({
    contactnumber: '',
    password: ''
  });

  const { contactnumber, password } = user;

  const onChange = e => setUser({ ...user, [e.target.name]: e.target.value });

  const onSubmit = e => {
    e.preventDefault();
    if (contactnumber === '' || password === '') {
      setAlert('Please fill in all fields', 'danger');
    } else {
      login({
        contactnumber,
        password
      });
    }
  }

  return (
    <Fragment>
      <div className="get-user">
        <div className="form-container">
          <div className="w3-row-padding">
            <div className="w3-half w3-margin-bottom">
              <form className="w3-container w3-card-4" onSubmit={onSubmit}>
                <p className="w3-wide w3-panel w3-black w3-opacity w3-center">Login client</p>
                
                <div className="w3-section">
                  <input 
                    className="w3-input" 
                    id="enterContactnumberInput"
                    type="number" 
                    name="contactnumber" 
                    placeholder="phone number here"
                    value={contactnumber}
                    onChange={onChange}
                  />
                </div>

                <div className="w3-section">
                  <input 
                      className="w3-input" 
                      id="enterPasswordInput"
                      type="password" required
                      name="password" 
                      placeholder="password here"
                      value={password}
                      onChange={onChange}
                    />
                </div>

                <div className="w3-section">
                  <p>
                  <button 
                    className="w3-button w3-block w3-black w3-margin-bottom"
                    id="userLoginButton"
                    type="submit">
                      Get Client
                  </button>
                  </p>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  )
}

export default LoginUserClient;
