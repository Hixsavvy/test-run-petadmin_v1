import React, { Fragment, useState, useContext, useEffect } from 'react';
import AlertContext from '../../../../context/alert/alertContext';
import AuthContext from '../../../../context/auth/authContext'; 

const RegUserClient = (props) => {
  const alertContext = useContext(AlertContext);
  const authContext = useContext(AuthContext);

  const { setAlert } = alertContext;
  const { register, error, clearErrors, isAuthenticated } = authContext;

  useEffect(() => {
    if (isAuthenticated) {
      props.history.push('/welcome');
    }

    if (error === 'User already exists') {
      setAlert(error, 'danger');
      clearErrors();
    }
    // eslint-disable-next-line
  }, [error, isAuthenticated, props.history]);

  const [user, setUser] = useState({
    firstname: '',
    lastname: '',
    contactnumber: '',
    email: '',
    password: '',
    password2: ''
  });

  const { firstname, lastname, contactnumber, email, password, password2 } = user;

  const onChange = e => setUser({ ...user, [e.target.name]: e.target.value });

  const onSubmit = e => {
    e.preventDefault();
    if (firstname === '' || lastname === '' || contactnumber === '' || password === '') {
      setAlert('Please enter all fields', 'danger');
    } else if (password !== password2) {
      setAlert('Passwords do not match', 'danger');
    } else {
      register({
        firstname, 
        lastname, 
        contactnumber, 
        email, 
        password
      });
    }
  };

  return (
    <Fragment>
      <div className="register-user">
        <div className="form-container">
          <div className="w3-row-padding">
            <div className="w3-half w3-margin-bottom">
              <form className="w3-container w3-card-4" onSubmit={onSubmit}>
              <p className="w3-wide w3-panel w3-black w3-opacity w3-center">Register client</p>

                <div className="w3-section">
                  <input 
                      className="w3-input" 
                      id="addFirstNameInput"
                      type="text" 
                      name="firstname" 
                      placeholder="first name here"
                      value={firstname}
                      onChange={onChange}
                    />
                </div>

                <div className="w3-section">
                  <input 
                      className="w3-input" 
                      id="addLastNameInput"
                      type="text" 
                      name="lastname" 
                      placeholder="last name here"
                      value={lastname}
                      onChange={onChange}
                    />
                </div>

                <div className="w3-section">
                  <input 
                    className="w3-input" 
                    id="addContactnumberInput"
                    type="number" 
                    name="contactnumber" 
                    placeholder="phone number here"
                    value={contactnumber}
                    onChange={onChange}
                  />
                </div>

                <div className="w3-section">
                  <input 
                    className="w3-input" 
                    id="addEmailInput"
                    type="email"
                    name="email" 
                    placeholder="email here"
                    value={email}
                    onChange={onChange}
                  />
                </div>

                <div className="w3-section">
                  <input 
                      className="w3-input" 
                      id="addPasswordInput"
                      type="password" required
                      name="password" 
                      placeholder="password here"
                      value={password}
                      onChange={onChange}
                      minLength="4"
                    />
                </div>

                <div className="w3-section">
                  <input 
                      className="w3-input" 
                      id="addConfirmPasswordInput"
                      type="password" required
                      name="password2" 
                      placeholder="confirm password here" 
                      value={password2}
                      onChange={onChange}
                      minLength="4"
                    />
                </div>

                <div className="w3-section">
                  <p>
                  <button 
                    className="w3-button w3-block w3-black w3-margin-bottom"
                    id="userRegisterButton"
                    type="submit">
                      Register
                  </button>
                  </p>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  )
}

export default RegUserClient;
