import React, { useContext, useRef, useEffect } from 'react';
import OwnBioContext from '../../../context/ownbio/ownbioContext';
 
const OwnBioFilter = () => {
  const ownbioContext = useContext(OwnBioContext);
  const text = useRef('');

  // Destructure 
  const { filterOwnBios, clearFilter, filtered } = ownbioContext;

  useEffect(() => {
    if (filtered == null) {
      text.current.value = '';
    }
  });

  const onChange = e => {
    if (text.current.value !== '') {
      filterOwnBios(e.target.value);
    } else {
      clearFilter(); 
    }
  }

  return (
    <form className="w3-container w3-card-4">

      <div className="w3-section">
        <input 
          className="w3-input" 
          id="filterOwnBiosTextInput"
          type="text" required
          placeholder="Filter Owner bios..." 
          ref={text}
          onChange={onChange}
        />
        {/*<label>Last name:</label>*/}
      </div>
{/*
      <div className="w3-section">
        <input 
          className="w3-input" 
          id="filterOwnBiosInput"
          type="number" required
          placeholder="Filter owner bios..." 
          ref={number}
          onChange={onChange}
        />
        <label>Phone:</label>
      </div>
*/}
    </form>
  )
}

export default OwnBioFilter;
