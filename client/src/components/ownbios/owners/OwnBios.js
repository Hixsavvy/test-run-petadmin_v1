import React, { Fragment, useContext, useEffect } from 'react';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import OwnBio from './OwnBio'; 
import Pet from '../pets/Pet'; 
//import CreatePet2BioForm from '../../ownbios/pets/petForms/CreatePet2BioForm'; 
import OwnBioContext from '../../../context/ownbio/ownbioContext'; 
import AuthContext from '../../../context/auth/authContext';
import PetContext from '../../../context/pet/petContext';
import Spinner from '../../navbar/Spinner';

const OwnBios = () => {
  const ownbioContext = useContext(OwnBioContext); 
  const { ownbio, getOwnBio, getPetData, loading } = ownbioContext;

  const authContext = useContext(AuthContext);
  const { user, loadUser } = authContext; 

  const petContext = useContext(PetContext);
  const { pet, setCurrentPet } = petContext; 

  useEffect(() => {
    if (user !== 0) {

      if (ownbio !== 0 && pet === 0) {
        getOwnBio();
      }
      else if (ownbio !== 0 && pet !== 0) {
        getOwnBio();
        setCurrentPet();
      }
      loadUser();
    }
    // eslint-disable-next-line
  }, []);

  // if (ownbio !== null && ownbio.length === 0 && !loading && pet !== null && pet.length === 0) {
  if (ownbio !== null && ownbio.length === 0 && !loading) {
    return <h5>add owners details from here</h5>
  } 
 
  return (
    <Fragment>
      <div style={ownbioStyle}>
         {ownbio !== null && !loading ? (
          <TransitionGroup>
            <CSSTransition 
              key={ownbio._id} 
              timeout={500} 
              classNames="bioItem"
            >
              <div>
              {/* <OwnBio ownbio={ownbio} /> */}
              <OwnBio ownbio={ownbio} pet={pet}/>
              </div>
            </CSSTransition>
          </TransitionGroup>
        ) 
        : <Spinner />}
      </div>
    </Fragment>
 
    // Use this below when we finally add filter functionality
    // <Fragment>
    //   {ownbios !== null && !loading ? (
    //     <TransitionGroup>
    //     {filtered !== null 
    //       ? filtered.map(ownbio => (
    //         <CSSTransition key={ownbio._id} timeout={500} classNames="bioItem">
    //           <OwnBio ownbio={ownbio} />
    //         </CSSTransition>
    //         )) 
    //       : ownbios.map(ownbio => (
    //         <CSSTransition key={ownbio._id} timeout={500} classNames="bioItem">
    //           <OwnBio ownbio={ownbio} />
    //         </CSSTransition>
    //         ))
    //     }
    //   </TransitionGroup>
    //   ) : <Spinner />}
    // </Fragment>
  );
};

const ownbioStyle = {
  display: "grid",
  gridTemplateColumns: "repeat(2, 2fr)",
  gridGap: "0rem"
}
 
export default OwnBios; 
