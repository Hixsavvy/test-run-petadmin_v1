import React, { Component } from 'react';


class OwnBioClient extends Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }
   
  handleSubmit(e) {
    e.preventDefault();
    this.props.onSubmit();
  } 
     
  render() {
    return (
      <div className="register-client">
        <div className="form-container">
          <div className="w3-row-padding">
            <div className="w3-half w3-margin-bottom">
              <form className="w3-container w3-card-4" onSubmit={this.props.onSubmit}>
                <h3>Add new client here</h3>
                <p>Enter client details.</p>

                <div className="w3-section">
                  <input 
                    className="w3-input" 
                    id="addFirstNameInput"
                    type="text" required
                    name="firstname" 
                    key="firstname"
                    placeholder="First name"
                    value={this.props.firstname}
                    onChange={this.props.onFirstNameChange}
                  />
                  <label>First name:</label>
                </div>

                <div className="w3-section">
                  <input 
                    className="w3-input" 
                    id="addLastNameInput"
                    type="text" required
                    name="lastname" 
                    key="lastname"
                    placeholder="Last name"
                    value={this.props.lastname}
                    onChange={this.props.onLastNameChange}
                  />
                  <label>Last name:</label>
                </div>

                <div className="w3-section">
                  <input 
                    className="w3-input" 
                    id="addContactnumberInput"
                    type="number" required
                    name="contactnumber" 
                    key="contactnumber"
                    placeholder="Please enter users phone number here"
                    value={this.props.contactnumber}
                    onChange={this.props.onContactNumberChange}
                  />
                  <label>Phone:</label>
                </div>

                <div className="w3-section">
                  <input 
                    className="w3-input" 
                    id="addEmailInput"
                    type="email"
                    name="email" 
                    key="email"
                    placeholder="Please enter users email here"
                    value={this.props.email}
                    onChange={this.props.onEmailChange}
                  />
                  <label>Email:</label>
                </div>

                <div className="w3-section">
                  <input 
                      className="w3-input" 
                      id="addPasswordInput"
                      type="password" required
                      name="password" 
                      key="password"
                      placeholder="Please enter users password here"
                      value={this.props.password}
                      onChange={this.props.onPasswordChange}
                    />
                    <label>Password:</label>
                </div>

                <div className="w3-section">
                  <input 
                      className="w3-input" 
                      id="addConfirmPasswordInput"
                      type="password" required
                      name="password2" 
                      key="password2"
                      placeholder="Please confirm password here"
                      value={this.props.password2}
                      onChange={this.props.onPassword2Change}
                    />
                    <label>Confirm password:</label>
                </div>

                <div className="w3-section">
                  <input 
                    className="w3-input" 
                    id="addVetNameInput"
                    type="text" required
                    name="vetname" 
                    key="vetname"
                    placeholder="Vet name"
                    value={this.props.vetname}
                    onChange={this.props.onVetnameChange}
                  />
                  <label>Vet name:</label>
                </div>
    
                <div className="w3-section">
                  <p>
                  <button 
                    className="w3-button w3-block w3-black w3-margin-bottom"
                    id="AddClientDataButton"
                    type="submit">
                      Next
                  </button>
                  </p>
                </div>

              </form>
            </div>   
          </div>
        </div>
      </div>
    );
  }
}

export default OwnBioClient; 