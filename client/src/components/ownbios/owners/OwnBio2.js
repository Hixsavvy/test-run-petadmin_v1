// This uses table display
import React, { Fragment, useContext } from 'react';
import PropTypes from 'prop-types';  
import OwnBioContext from '../../../context/ownbio/ownbioContext';
import AuthContext from '../../../context/auth/authContext'; 
//import  AuthWelcomeButtons from '../../events/AuthWelcomeButtons'; 

const OwnBio2 = ({ownbio}) => {
  const ownbioContext = useContext(OwnBioContext); 
  const { deleteOwnBio, setCurrent, clearOwnBio } = ownbioContext;

  const authContext = useContext(AuthContext);
  const { user } = authContext;

  const { _id, address, vetname, specialneeds } = ownbio;

  const { contactnumber, email } = user;

  const onDelete = () => {
    deleteOwnBio(_id);
    clearOwnBio();
  };

  return (
    <Fragment>
      <div className="w3-container">
        <div className="w3-center">
          <h3>Owners Bio</h3>
        </div>
        <div className="w3-responsive w3-card-4">
          <table className="w3-table w3-striped w3-bordered">
            {/* <thead>
              <tr className="w3-theme">
                <th>Client</th>
                <th>Owner Bio</th>
                <th>Pet Bio</th>
                <th>Dates</th>
              </tr>
            </thead> */}
            {/* <tbody>
              <tr>
              <td>Client</td>
              <td>Owner Bio</td>
              <td>Pet Bio</td>
              <td>Dates</td>
              </tr>
            </tbody> */}
            <tbody>
              <tr>
              <td>
                <div className="card bg-light">
                  <h6 className="text-primary text-left fas fa-user">{' '}{user.firstname}{' '}{user.lastname}</h6>
                  <ul className="list">
                      {contactnumber && (
                        <li>
                          <i className="fas fa-phone"></i> {' '}{user.contactnumber}
                        </li>
                      )}
                    </ul>
                    <ul className="list">
                      {email && (
                        <li>
                          <i className="fas fa-envelope-open"> {' '}</i> {user.email}
                        </li>
                      )}
                  </ul>
                </div>
              </td>
              <td>
              <div className="card bg-light">
                <ul className="list">
                  {vetname && (
                    <li>
                      <i className="text-primary text-left">Vet:{' '}</i> {vetname}
                    </li>
                  )}
                </ul> 
                <ul className="list">
                  {specialneeds && (
                    <li>
                      <i className="text-primary text-left">Specialneeds:{' '}</i> {specialneeds}
                    </li>
                  )}
                </ul>
                </div>
              </td>
              {/* <td>Pet Bio</td> */}
              {/* <td>
              <div className="card bg-light">
                <ul className="list">
                  {address && (
                    <li>
                      <i className="fa fa-map-marker">{' '}</i> {ownbio.address}
                    </li>
                  )}
                </ul> 
                <a href={pets.datecalc} className="btn btn-dark btn-link my-1">Dates</a>
              </div>
              </td> */}
              </tr>
            </tbody>
          </table>
        
        </div>
        
        {/* <div className="card bg-light">
        <p>
          <AuthWelcomeButtons />
          <button className="btn btn-dark btn-sm" onClick={() => setCurrent(ownbio)}>Edit</button>
          <button className="btn btn-danger btn-sm" onClick={onDelete}>Delete</button>
      </div> */}
    </div>
  </Fragment>
  );
};

{/* avatar is not required, so proptypes '!isRequired at the moment */}
OwnBio.propTypes = {
  ownbio: PropTypes.object.isRequired
}

export default OwnBio2;