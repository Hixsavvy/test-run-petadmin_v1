// This uses grid display
import React, { Fragment, useContext } from 'react';
import PropTypes from 'prop-types';  
import OwnBioContext from '../../../context/ownbio/ownbioContext';
import AuthContext from '../../../context/auth/authContext';
import PetContext from '../../../context/pet/petContext'; 
//import  AuthWelcomeButtons from '../../events/AuthWelcomeButtons';

const OwnBio = ({ownbio}) => {
  const ownbioContext = useContext(OwnBioContext); 
  const { deleteOwnBio, setCurrent, clearOwnBio } = ownbioContext;

  const authContext = useContext(AuthContext);
  const { user } = authContext; 

  const petContext = useContext(PetContext);
  const { pet, petbio } = petContext;

  const { _id, address, vetname } = ownbio;

  const { contactnumber, email } = user;

  // const { petbio } = pet; 

  const onDeleteOwnBio = () => {
    deleteOwnBio(_id);
    clearOwnBio();
  };

  return (
    <Fragment>
      <div className="card bg-light">
        <h6 className="text-primary text-left fas fa-user">{' '}{user.firstname}{' '}{user.lastname}</h6>
        {/* <h6 className="text-primary text-left fas fa-user">{' '}{user.firstname}</h6> */} 
        <ul className="list">
          {contactnumber && (
            <li>
              <i className="fas fa-phone"></i> {' '}{user.contactnumber}
            </li>
          )}
        </ul>
        <ul className="list">
          {email && (
            <li>
              <i className="fas fa-envelope-open"> {' '}</i> {user.email}
            </li>
          )}
        </ul>
      </div>
 
      <div className="card bg-light">
        <ul className="list">
          {petbio && (
            <li>
              <i className="fa fa-map-marker">{' '}</i> {pet.petbio}
            </li>
          )}
        </ul>
      </div>
        
      <div className="card bg-light">
        <ul className="list">
          {address && (
            <li>
              <i className="fa fa-map-marker">{' '}</i> {ownbio.address}
            </li>
          )}
        </ul>
      </div> 

      {/* <div className="card bg-light">
        <a href={pets.datecalc} className="btn btn-dark btn-link my-1">Dates</a>
      </div>*/}
        
      <div className="card bg-light">
        <ul className="list">
          {vetname && (
            <li>
              <i className="text-primary text-left">Vet:{' '}</i> {vetname}
            </li>
          )}
        </ul> 
        {/* <ul className="list">
          {specialneeds && (
            <li>
              <i className="text-primary text-left">Specialneeds:{' '}</i> {specialneeds}
            </li>
          )}
        </ul> */}
      </div>

      {/* <div className="card bg-light">
        <p>
          <AuthWelcomeButtons />
          <button className="btn btn-dark btn-sm" onClick={() => setCurrent(ownbio)}>Edit</button>
          <button className="btn btn-danger btn-sm" onClick={onDeleteOwnBio}>Delete</button>
      </div> */}
  </Fragment>
  );
};

{/* avatar is not required, so proptypes '!isRequired at the moment */}
OwnBio.propTypes = {
  ownbio: PropTypes.object.isRequired,
  pet: PropTypes.object.isRequired
}

export default OwnBio;