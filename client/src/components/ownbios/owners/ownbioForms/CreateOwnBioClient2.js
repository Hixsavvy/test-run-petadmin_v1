// Form is solely for adding owner bio. NOT for editing and update 

import React, { Fragment, useState, useContext } from 'react';
import OwnBioContext from '../../../../context/ownbio/ownbioContext';
//import AuthContext from '../../../../context/auth/authContext'; 

const CreateOwnBioClient2 = () => {
  const ownbioContext = useContext(OwnBioContext); 

  const { addOwnBio } = ownbioContext; 

  const [ownbio, setOwnBio] = useState({
    //age: '',
    vetname: '',
    // specialneeds: '',
    //type: ''
  });

  const { vetname } = ownbio;

  const onChange = e => 
    setOwnBio({ 
      ...ownbio,  
      [e.target.name]: e.target.value 
    }); 

  const onSubmit = async e => {
    e.preventDefault();
    addOwnBio(ownbio); 
    setOwnBio({
      //age: '',
      vetname: '',
      // specialneeds: ''
    });
  };

  // const clearAll = () => {
  //   clearCurrent(); 
  // } 
    
  return (
    <Fragment>
      <div className="register-owner-bio">
        <div className="form-container">
          <div className="w3-row-padding">
            <div className="w3-half w3-margin-bottom">
              
              <form className="w3-container w3-card-4" onSubmit={onSubmit}>
                <div className="w3-container w3-border">
                {/* <h3 className="w3-panel w3-black w3-opacity">{current ? "Edit owner bio" : "Add owner bio"}</h3> */}
                <p className="w3-wide w3-panel w3-black w3-opacity w3-center">Add owner details</p>
                </div>
{/*
                <div className="w3-section">
                  <input 
                      className="w3-input" 
                      id="addPetsAgeInput"
                      type="text" 
                      name="age" 
                      placeholder="pets age here"
                      value={age}
                      onChange={onChange}
                  /> 
                </div>
*/}
                <div className="w3-section">
                  <input 
                      className="w3-input" 
                      id="addVetNameInput"
                      type="text" required
                      name="vetname" 
                      placeholder="vets name here"
                      value={vetname}
                      onChange={onChange}
                    />
                </div>
 
                {/* <div className="w3-section">
                  <input 
                    className="w3-input" 
                    id="addSpecialNeedsInput"
                    type="textarea" required
                    name="specialneeds" 
                    placeholder="Any special needs for pet?"
                    value={specialneeds}
                    onChange={onChange}
                  />
                </div> */} 

                {/* <div className="w3-section">
                  <h6>Cat/Dog or Other</h6>
                  <input 
                    className="w3-input" 
                    id="addSpecialNeedsInput"
                    type="radio" required
                    name="type" 
                    placeholder="Any special needs for pet?"
                    value={cat/dog}
                    checked={type === "cat/dog"}
                  />
                  Cat / Dog{' '}
                </div>

                <div className="w3-section">
                  <h6>Cat/Dog or Other</h6>
                  <input 
                    className="w3-input" 
                    id="addSpecialNeedsInput"
                    type="radio" required
                    name="type" 
                    placeholder="Any special needs for pet?"
                    value={other}
                    checked={type === "other"}
                  />
                  Other{' '}
                </div> */}

                <div className="w3-section">
                  <p>
                  <input 
                    className="w3-button w3-block w3-black w3-margin-bottom"
                    id="OwnBioForm1Button"
                    type="submit"
                    // value={current ? "Update owner bio" : "Add owner bio"}
                    Add owner bio
                  >
                  </input>
                  </p>
                </div>

                {/* <div className="w3-section">
                  <p>
                  <input 
                    className="w3-button w3-block w3-black w3-margin-bottom"
                    id="OwnBioForm1Button"
                    type="submit"
                    value={current ? "Update owner bio" : "Add owner bio"}
                  >
                  </input>
                  </p>
                </div> */}
                {/* {current && (
                  <div className="w3-section">
                    <button 
                    className="w3-button w3-block w3-light-blue w3-margin-bottom"
                    id="ClearOwnBioForm1Button"
                    type="submit" onClick={clearAll}>
                      Clear 
                    </button>
                  </div>
                )} */}
              </form>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default CreateOwnBioClient2;