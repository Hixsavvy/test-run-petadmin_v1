// This is the original form with experimenting 
import React, { Fragment, useContext, useEffect } from 'react';
import AuthContext from '../../../context/auth/authContext';
import OwnBioContext from '../../../context/ownbio/ownbioContext';
import PetContext from '../../../context/pet/petContext'; 
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import OwnBio from './OwnBio'; 
//import Pet from '../pets/Pet'; 
import Spinner from '../../navbar/Spinner';

const OwnBios1 = () => {
  const ownbioContext = useContext(OwnBioContext); 
  const { ownbio, getOwnBio, loading } = ownbioContext;

  const authContext = useContext(AuthContext);
  const { loadUser } = authContext;

  const petContext = useContext(PetContext);
  const { _id, pet, getPet, setCurrentPet } = petContext;

  
  useEffect(() => {
    getOwnBio();
    loadUser();
    getPet(_id);
    setCurrentPet(); 
    // eslint-disable-next-line
  }, []);

  // const noOwnBioMeansNoPet = () => {
  //   ownbio !== null && ownbio.length === 0 && !loading
  // };

  // const ownBioHasAPet = () => {
  //   ownbio !== null && ownbio.length === 0 && pet !== null && pet.length === 0 && !loading
  // };

  const ownbioHasPet = (
    <Fragment>
      <div style={bothStyle} classNames="bioItem">
        <OwnBio ownbio={ownbio} key={ownbio._id} display="gridA" />
        {/*<OwnBio ownbio={ownbio} pet={pet} display="gridB" />*/}
        <OwnBio pet={pet} display="gridB" /> 
      : <Spinner />
    </div>
    </Fragment>
  ); 

  // const ownbioHasPet1 = (
  //   <Fragment>
  //     <div style={bothStyle}>
  //     {/* {ownbio !== null && pet !== null && !loading ? ( */}
  //       <TransitionGroup>
  //         <CSSTransition 
  //           //key={ownbio._id} 
  //           timeout={500} 
  //           display="gridA"
  //           classNames="bioItem"
  //         >
  //           <OwnBio ownbio={ownbio} />
  //         </CSSTransition>
  //         <CSSTransition 
  //           key={pet._id} 
  //           timeout={500} 
  //           display="gridC"
  //           classNames="bioItem"
  //         >
  //           <OwnBio ownbio={ownbio} pet={pet} />
  //            {/*<OwnBio pet={pet} /> */}
  //         </CSSTransition>
  //       </TransitionGroup>
  //     {/* ) */}
  //     : <Spinner />
  //     {/* }  */}
  //   </div>
  //   </Fragment>
  // );

  const ownbioHasNoPet = (
    <Fragment>
      <div style={ownbioStyle}>
        <OwnBio ownbio={ownbio} />
        : <Spinner />
      </div>
    </Fragment>
  )

  // const ownbioHasNoPet1 = (
  //   <Fragment>
  //     <div style={ownbioStyle}>
  //       {/* {ownbio !== null && !loading ? ( */}
  //         <TransitionGroup>
  //           <CSSTransition 
  //             key={ownbio._id} 
  //             timeout={500} 
  //             classNames="bioItem"
  //           >
  //             <OwnBio ownbio={ownbio} />
  //           </CSSTransition>
  //         </TransitionGroup>
  //       {/* )  */}
  //       : <Spinner />
  //       {/* } */}
  //     </div>
  //   </Fragment>
  // )

  if (ownbio !== null && ownbio.length === 0 && !loading) {
    return <h5>add owners details from here</h5>
  } 

  // if (noOwnBioMeansNoPet) {
  //   return <h5>add owners details from here</h5>
  // }

  // else if (ownBioHasPet) {
  //   return <Fragment>
  //   <div style={bothStyle}>
  //     {ownbio !== null && pet !== null && !loading ? (
  //       <TransitionGroup>
  //         <CSSTransition 
  //           key={ownbio._id} 
  //           timeout={500} 
  //           display="gridA"
  //           classNames="bioItem"
  //         >
  //           <OwnBio ownbio={ownbio} />
  //         </CSSTransition>
  //         <CSSTransition 
  //           key={pet._id} 
  //           timeout={500} 
  //           display="gridC"
  //           classNames="bioItem"
  //         >
  //           <OwnBio ownbio={ownbio} pet={pet} />
  //           {/* <OwnBio pet={pet} /> */}
  //         </CSSTransition>
  //       </TransitionGroup>
  //     ) 
  //     : <Spinner />}
  //   </div>
  //   {/* <div style={petStyle}>
  //     {ownbio !== null && pet !== null && !loading ? (
  //       <TransitionGroup>
  //         <CSSTransition 
  //           key={ownbio._id} 
  //           timeout={500} 
  //           classNames="bioItem"
  //         ></CSSTransition>
  //         <CSSTransition 
  //           key={pet._id} 
  //           timeout={500} 
  //           classNames="bioItem"
  //         >
  //           <OwnBio pet={pet} />
  //         </CSSTransition>
  //       </TransitionGroup>
  //     ) 
  //     : <Spinner />}
  //   </div> */}
  // </Fragment>
  // }


  return (
    <Fragment>
      {ownbio !== null && pet !== null && !loading ? ownbioHasPet : ownbioHasNoPet }
    </Fragment>
    // <Fragment>
    //   {ownbios !== null && !loading ? (
    //     <TransitionGroup>
    //     {filtered !== null 
    //       ? filtered.map(ownbio => (
    //         <CSSTransition key={ownbio._id} timeout={500} classNames="bioItem">
    //           <OwnBio ownbio={ownbio} />
    //         </CSSTransition>
    //         )) 
    //       : ownbios.map(ownbio => (
    //         <CSSTransition key={ownbio._id} timeout={500} classNames="bioItem">
    //           <OwnBio ownbio={ownbio} />
    //         </CSSTransition>
    //         ))
    //     }
    //   </TransitionGroup>
    //   ) : <Spinner />}
    // </Fragment>
  );
};

const ownbioStyle = {
  display: "gridA",
  gridTemplateColumns: "repeat(2, 2fr)",
  gridGap: "0rem"
}

const petStyle = {
  display: "gridB",
  gridTemplateColumns: "repeat(2, 2fr)",
  gridGap: "0rem"
}

const bothStyle = {
  display: "gridA",
  gridTemplateColumns: "repeat(2, 2fr)",
  gridGap: "0rem",
  display: "gridB",
  gridTemplateColumns: "repeat(3, 2fr)",
  gridGap: "0rem"
}
 
export default OwnBios1;
