import React, { Component } from 'react';
 
import axios from 'axios'; 

import PetRegClient from '../owners/pets/PetRegClient'; 
import AddressRegClient from '../common/elements/AddressRegClient';
import SuccessRegClient from '../common/success/SuccessRegClient';

class OwnerBioForm extends Component {
  constructor() {
    super();
    this.state = {
      step: 1,

      id: '',
      
      address_house: '',
      address_street: '',
      address_street2: '',
      address_post_code: '',
      address_city: '',

      //pets: {},

      petname: '',
      pettype: '',
      petbreed: '',
      age: '',
      avatar: '',
      
      errors: {}
    };
 
    this.nextStep = this.nextStep.bind(this); 
    this.prevStep = this.prevStep.bind(this);
    this.resetForm = this.resetForm.bind(this);

  }

  // Reset form after submission
   resetForm = () => {
    const { step } = this.state;
    this.setState({
      step: 2 + 1,
      //step: 3 + 1,
      //nextStep: {}
      fields: {
        id: ''
      } 
    })
  };

  prevStep = () => {
    const { step } = this.state
    this.setState({
      step: step - 1
    })
  }

  nextStep = () => {
    const { step } = this.state;
    if (step !==2) {
      this.setState({ step: step + 1 });
    } else {
      alert("Submitting");
      this.resetForm();
      
      const newOwner = {

        id: this.params.id,
        //this.state.id,

        address: {
          house: this.state.address_house,
          street: this.state.address_street,
          street2: this.state.address_street2,
          post_code: this.state.address_post_code,
          city: this.state.address_city
        },

        // petname: this.state.petname,
        // pettype: this.state.pettype,
        // petbreed: this.state.petbreed, 

        pets: {
          petname: this.state.petname,
          pettype: this.state.pettype,
          petbreed: this.state.petbreed,
          age: this.state.age,
          avatar: this.state.avatar
        },
 
        errors: {}
      };
     
      axios.post('/api/v1/owners/register', newOwner)
        .then(res => console.log(res.data))
        .catch(err => console.log(err));

    }
  }
 
    onChange(field) {
      return (e) => this.setState({ [field]: e.target.value });
    }

    render() {
      switch(this.state.step) {
        case 1:
          return (
            <PetRegClient 
              key="pets"
              onSubmit={this.nextStep}
              petname={this.state.petname}
              pettype={this.state.pettype}
              petbreed={this.state.petbreed}
              age={this.state.age}
              onChange={this.onChange}
              //onPetNameChange={this.onChange("petname")}
              //onPetTypeChange={this.onChange("pettype")}
              //onPetBreedChange={this.onChange("petbreed")}
              //onAgeChange={this.onChange("age")}
            />
          );
        case 2:
          return (
            <AddressRegClient
              key="address"
              onSubmit={this.nextStep}
              addressHouse={this.state.address_house}
              addressStreet={this.state.address_street}
              addressStreet2={this.state.address_street2}
              addressPostCode={this.state.address_post_code}
              addressCity={this.state.address_city}
              onChange={this.onChange}
              //onAddressChangeHouse={this.onChange("address_house")}
              //onAddressChangeStreet={this.onChange("address_street")}
              // onAddressChangeStreet2={this.onChange("address_street2")}
              // onAddressChangePostCode={this.onChange("address_post_code")}
              // onAddressChangeCity={this.onChange("address_city")}
            />
          );
        case 3: 
          return (
            <SuccessRegClient
              key="success" 
            />
          );
      }
    }
}
 
export default OwnerBioForm;