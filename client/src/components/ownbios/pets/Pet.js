import React, { Fragment, useContext } from 'react';
import PropTypes from 'prop-types';
import PetContext from '../../../context/pet/petContext';
// import AuthContext from '../../../context/auth/authContext';
import OwnBioContext from '../../../context/ownbio/ownbioContext'; 

const Pet = ({pet}) => {
  const petContext = useContext(PetContext);
  const { getPet, deletePet, clearPet, getPetBio } = PetContext;

  // const authContext = useContext(AuthContext);
  // const { user } = authContext;

  const ownbioContext = useContext(OwnBioContext);
  const { vetname } = ownbioContext; 

  const { _id, petbio, petname, petbreed, specialneeds } = pet;

  const myPet = () => {
    //getPet();
    getPetBio();
  }; 

  const onDeletePet = () => {
    deletePet(_id);
    clearPet();
  };

  return (
    <Fragment>
      <div className="card bg-light">
        <h6 className="text-primary text-left fas fa-dog">{' '}{pet.petbio.petname}</h6>
        {/* <ul className="list">
          {petbreed && (
            <li>
              <i className="fas fa-phone"></i> {' '}{petbio.contactnumber}
            </li>
          )}
        </ul> */}
        <ul className="list">
          {petbreed && (
            <li>
              <i className="text-primary text-left">Breed:{' '}</i> {pet.petbio.petbreed}
            </li>
          )}
        </ul>
        <ul className="list">
          {specialneeds && (
            <li>
              <i className="text-primary text-left">Specialneeds?:{' '}</i> {pet.petbio.specialneeds}
            </li>
          )}
        </ul>
        <ul className="list">
          {vetname && (
            <li>
              <i className="text-primary text-left">Vet:{' '}</i> {vetname}
            </li>
          )}
        </ul>
      </div>
    </Fragment>
  )
}; 

Pet.propTypes = {
  pet: PropTypes.object.isRequired
}

export default Pet;