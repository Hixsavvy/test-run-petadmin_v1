import React, { Component } from 'react';
//import { Link } from 'react-router-dom';
 
class PetRegClient extends Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }
   
  handleSubmit(e) {
    e.preventDefault();
    this.props.onSubmit(); 
  }

  render() {
    return (
      <div className="register-pet">
        <div className="form-container">
          <div className="w3-row-padding">
            <div className="w3-half w3-margin-bottom">
              <form className="w3-container w3-card-4" onSubmit={e => onSubmit(e)}>
                {/*<h3>Add new pet here</h3>*/}
                <p>Please enter pet details.</p>

                <div className="w3-section">
                  <input 
                    className="w3-input" 
                    id="addPetNameInput"
                    type="text" required
                    name="petname" 
                    placeholder="Pet name"
                    value={petname}
                    onChange={e => onChange(e)}
                  />
                  <label>Pet name:</label>
                </div>

                <div className="w3-section">
                  <input 
                    className="w3-input" 
                    id="addPetTypeInput"
                    type="text" required
                    name="pettype" 
                    placeholder="Pet type"
                    value={pettype}
                    onChange={e => onChange(e)}
                  />
                  <label>Pet type:</label>
                </div>

                <div className="w3-section">
                  <input 
                    className="w3-input" 
                    id="addPetBreedInput"
                    type="text" required
                    name="petbreed" 
                    placeholder="Pet breed"
                    value={petbreed}
                    onChange={e => onChange(e)}
                  />
                  <label>Pet breed:</label>
                </div> 

                <div className="w3-section">
                  <input 
                    className="w3-input" 
                    id="addPetAgeInput"
                    type="text" required
                    name="age" 
                    placeholder="Pet age in months, or years"
                    value={age}
                    onChange={e => onChange(e)}
                  />
                  <label>Pet age:</label>
                </div> 
    
                <div className="w3-section">
                  <p>
                  <button 
                    className="w3-button w3-block w3-black w3-margin-bottom"
                    id="AddPetButton"
                    type="submit">
                      Next
                  </button>
                  </p>
                </div>

              </form>
            </div>   
          </div>
        </div>
      </div>
    );
  }
}
 
export default PetRegClient;