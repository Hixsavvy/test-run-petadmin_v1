import React, { Fragment, useContext, useEffect } from 'react';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import Pet from '../../ownbios/pets/Pet';
//import OwnBio from './OwnBio'; 
import AuthContext from '../../../context/auth/authContext';
import PetContext from '../../../context/pet/petContext';
import Spinner from '../../navbar/Spinner';

const Pets = () => {

  const petContext = useContext(PetContext); 
  const { pet, getPet, setCurrentPet, loading } = petContext;  

  const authContext = useContext(AuthContext);
  const { loadUser } = authContext;

  //const ownbioContext = useContext(OwnBioContext); 
  //const { ownbio, getOwnBio, loading } = ownbioContext; 

  const myPet = () => {
    getPet();
  };
  
  useEffect(() => {
    getPet();
    //myPet(); 
    //setCurrentPet();
    loadUser();
    //getOwnBio();
     
    // eslint-disable-next-line
  }, []);

  if (pet !== null && pet.length === 0 && !loading) {
  //if (ownbio !== null && ownbio.length === 0 && !loading) {
  // Load Pet || PetBio form(s) here, if...
  // return <h5>add owners details from here</h5> 
    return <h5>add pet details from here</h5>
  } 

  return (
    <Fragment>
      <div style={petbioStyle}>
        {pet !== null && !loading ? (
          <TransitionGroup>
            <CSSTransition
              key={pet._id} 
              //key={pet.pet._id} 
              //key={pet.petbio._id}  
              timeout={500} 
              classNames="bioItem"
            >
              {/*<OwnBio petbio={pet.petbio} />*/}
              {/* <OwnBio pet={pet.petbio} /> */}
               <Pet pet={pet.petbio} /> 
            </CSSTransition>
          </TransitionGroup>
        ) 
        : <Spinner />}
      </div>
    </Fragment>
  );
}; 

const petbioStyle = {
  display: "grid",
  gridTemplateColumns: "repeat(2, 4fr)",
  gridGap: "0rem"
}
 
export default Pets; 
