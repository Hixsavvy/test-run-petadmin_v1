// Form is solely for adding pet. NOT (yet) for editing and update 

import React, { Fragment, useState, useContext } from 'react';
//import OwnBioContext from '../../../../context/ownbio/ownbioContext'; 
import PetContext from '../../../../context/pet/petContext'; 

const CreatePetForm = () => {
  //const ownbioContext = useContext(OwnBioContext); 

  //const { addOwnBio } = ownbioContext;

  const petContext = useContext(PetContext);

  const { addPet } = petContext;


  const [pet, setPet] = useState({
    age: ''
  });

  const { age } = pet;

  //const { petname, pettype, petbreed } = petbio;

  //const { age, petbio: [petname, pettype, petbreed] } = pet;

  const petChange = e => 
    setPet({ 
      ...pet,  
      [e.target.name]: e.target.value 
  }); 
    
  const onSubmit = async e => {
    e.preventDefault();
    addPet(pet);
    setPet({
      age: ''
    });
  };

  // const clearAll = () => {
  //   clearCurrent(); 
  // } 
    
  return (
    <Fragment>
      <div className="register-pet-petbio">
        <div className="form-container">
          <div className="w3-row-padding">
            <div className="w3-half w3-margin-bottom">
              
              <form className="w3-container w3-card-4" onSubmit={onSubmit}>
                <div className="w3-container w3-border">
                {/* <h3 className="w3-panel w3-black w3-opacity">{current ? "Edit owner bio" : "Add owner bio"}</h3> */}
                <p className="w3-wide w3-panel w3-black w3-opacity w3-center">Add pet details</p>
                </div>
{/**/}
                <div className="w3-section">
                  <input 
                      className="w3-input" 
                      id="addPetsAgeInput"
                      type="text" required
                      name="age" 
                      placeholder="pets age here"
                      value={age}
                      onChange={petChange}
                  /> 
                </div>
{/*
                <div className="w3-section">
                  <input 
                      className="w3-input" 
                      id="addPetNameInput"
                      type="text" required
                      name="petname" 
                      placeholder="pets name here"
                      value={petname}
                      onChange={onChange}
                      // onChange={petbioChange}
                    />
                </div>

                <div className="w3-section">
                  <input 
                    className="w3-input" 
                    id="addPetTypeInput"
                    type="text" required
                    name="pettype" 
                    placeholder="pet type here"
                    value={pettype}
                    onChange={onChange}
                    // onChange={petbioChange}
                  />
                </div>

                <div className="w3-section">
                  <input 
                    className="w3-input" 
                    id="addPetBreedInput"
                    type="text" required
                    name="petbreed" 
                    placeholder="pet breed here"
                    value={petbreed}
                    onChange={onChange}
                    // onChange={petbioChange}
                  />
                </div>

                 <div className="w3-section">
                  <h6>Cat/Dog or Other</h6>
                  <input 
                    className="w3-input" 
                    id="addSpecialNeedsInput"
                    type="radio" required
                    name="type" 
                    placeholder="Any special needs for pet?"
                    value={cat/dog}
                    checked={type === "cat/dog"}
                  />
                  Cat / Dog{' '}
                </div>

                <div className="w3-section">
                  <h6>Cat/Dog or Other</h6>
                  <input 
                    className="w3-input" 
                    id="addSpecialNeedsInput"
                    type="radio" required
                    name="type" 
                    placeholder="Any special needs for pet?"
                    value={other}
                    checked={type === "other"}
                  />
                  Other{' '}
                </div> */}

                <div className="w3-section">
                  <p>
                  <input 
                    className="w3-button w3-block w3-black w3-margin-bottom"
                    id="PetFormButton"
                    type="submit"
                    // value={current ? "Update pet details" : "Add pet details"}
                    Add pet details
                  >
                  </input>
                  </p>
                </div>

                {/* <div className="w3-section">
                  <p>
                  <input 
                    className="w3-button w3-block w3-black w3-margin-bottom"
                    id="PetFormButton"
                    type="submit"
                    value={current ? "Update details" : "Add pet details"}
                  >
                  </input>
                  </p>
                </div> */}
                {/* {current && (
                  <div className="w3-section">
                    <button 
                    className="w3-button w3-block w3-light-blue w3-margin-bottom"
                    id="ClearPetFormButton"
                    type="submit" onClick={clearAll}>
                      Clear 
                    </button>
                  </div>
                )} */}
              </form>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default CreatePetForm; 