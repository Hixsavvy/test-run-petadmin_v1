// Form is solely for adding pet through a petbio. For now, NOT for editing and update 

import React, { Fragment, useState, useContext } from 'react';
//import OwnBioContext from '../../../../context/ownbio/ownbioContext';
import PetContext from '../../../../context/pet/petContext'; 

const CreatePet2BioForm = () => {
  //const ownbioContext = useContext(OwnBioContext); 

  //const { addOwnBio } = ownbioContext;

  const petContext = useContext(PetContext);

  const { addPet } = petContext;

  const [petbio, setPetBio] = useState({
    petbio: [
      {
        age: '',
        petname: '',
        pettype: '',
        petbreed: '',
        firsteverarrivaldate: '',
        isBoarder: '',
        specialneeds: ''
      } 
    ]
  });

  const { age, petname, pettype, petbreed, firsteverarrivaldate, specialneeds, isBoarder } = petbio;

  const petbioChange = e => 
  setPetBio({ 
    ...petbio,  
    [e.target.name]: e.target.value 
  });

  const onSubmit = async e => {
    e.preventDefault();
    //addPet(pet);
    addPet(petbio);
    //addPetOwnBio(pet);
    setPetBio({
      age: '',
      petname: '',
      pettype: '',
      petbreed: '',
      firsteverarrivaldate: '',
      isBoarder: '',
      specialneeds: ''
    });
  };

  // const clearAll = () => {
  //   clearCurrent();
  // } 
    
  return (
    <Fragment>
      <div className="register-pet-petbio">
        <div className="form-container">
          <div className="w3-row-padding">
            <div className="w3-half w3-margin-bottom">
              
              <form className="w3-container w3-card-4" onSubmit={onSubmit}>
                <div className="w3-container w3-border">
                {/* <h3 className="w3-panel w3-black w3-opacity">{current ? "Edit owner bio" : "Add owner bio"}</h3> */}
                <p className="w3-wide w3-panel w3-black w3-opacity w3-center">Add pet details</p>
                </div>

                <div className="w3-section">
                  <input 
                      className="w3-input" 
                      id="addPetsAgeInput"
                      type="text" 
                      name="age" 
                      placeholder="pets age here"
                      value={age || ''}
                      // onChange={onChange}
                      // onChange={petChange}
                      onChange={petbioChange}
                  /> 
                </div>
 
                <div className="w3-section">
                  <input 
                      className="w3-input" 
                      id="addPetNameInput"
                      type="text" required
                      name="petname" 
                      placeholder="pets name here"
                      value={petname || ''}
                      onChange={petbioChange}
                    />
                </div>

                <div className="w3-section">
                  <input 
                    className="w3-input" 
                    id="addPetTypeInput"
                    type="text" required
                    name="pettype" 
                    placeholder="pet type here"
                    value={pettype || ''}
                    onChange={petbioChange}
                  />
                </div>

                <div className="w3-section">
                  <input 
                    className="w3-input" 
                    id="addPetBreedInput"
                    type="text" required
                    name="petbreed" 
                    placeholder="pet breed here"
                    value={petbreed || ''}
                    onChange={petbioChange}
                  />
                </div>

                <div className="w3-section">
                  <input 
                    className="w3-input" 
                    id="addSpecialNeedsInput"
                    type="textarea" required
                    name="specialneeds" 
                    placeholder="Any special needs for pet?"
                    value={specialneeds || ''}
                    onChange={petbioChange}
                  />
                </div>

{/*
                <div className="w3-section">
                  <input 
                      className="w3-input" 
                      id="addFirstArrivalDateInput"
                      type="date" 
                      name="firsteverarrivaldate" 
                      placeholder="first ever arrival date here"
                      value={firsteverarrivaldate}
                      onChange={onChange}
                      // onChange={petChange}
                  /> 
                </div>
*/}

                {/* <div className="w3-section">
                  <h6>Daily or Boarder</h6>
                  <input 
                    className="w3-input" 
                    id="addIsBoarderInput"
                    type="radio" required
                    name="isBoarder" 
                    placeholder="Daily or Boarder?"
                    value={daily}
                    checked={type === "daily"}
                  />
                  Daily{' '}
                </div>

                <div className="w3-section">
                  <h6>Daily or Boarder</h6>
                  <input 
                    className="w3-input" 
                    id="addIsBoarderInput2"
                    type="radio" required
                    name="isBoarder" 
                    placeholder="Any special needs for pet?"
                    value={boarder}
                    checked={type === "boarder"}
                  />
                  Boarder{' '}
                </div> */}

                <div className="w3-section">
                  <p>
                  <input 
                    className="w3-button w3-block w3-black w3-margin-bottom"
                    id="Pet2BioFormButton"
                    type="submit"
                    // value={current ? "Update pet details" : "Add pet details"}
                    Add pet details
                  >
                  </input>
                  </p>
                </div>

                {/* <div className="w3-section">
                  <p>
                  <input 
                    className="w3-button w3-block w3-black w3-margin-bottom"
                    id="Pet2BioFormButton"
                    type="submit"
                    value={current ? "Update details" : "Add pet details"}
                  >
                  </input>
                  </p>
                </div> */}
                {/* {current && (
                  <div className="w3-section">
                    <button 
                    className="w3-button w3-block w3-light-blue w3-margin-bottom"
                    id="ClearPet2BioFormButton"
                    type="submit" onClick={clearAll}>
                      Clear 
                    </button>
                  </div>
                )} */}
              </form>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default CreatePet2BioForm;