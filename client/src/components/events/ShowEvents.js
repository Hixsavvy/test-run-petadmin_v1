import React, { Fragment, useContext, useEffect } from 'react'; 
import OwnBios from '../ownbios/owners/OwnBios';
//import Pets from '../ownbios/pets/Pets';
import CreateOwnBioClient from '../ownbios/owners/ownbioForms/CreateOwnBioClient';
import CreatePet2BioForm from '../../components/ownbios/pets/petForms/CreatePet2BioForm';
import OwnBioFilter from '../ownbios/owners/OwnBioFilter';  
import AuthContext from '../../context/auth/authContext';
import OwnBioContext from '../../context/ownbio/ownbioContext';
import PetContext from '../../context/pet/petContext'; 

const ShowEvents = () => {
  const authContext = useContext(AuthContext);
  const { loading } = authContext;

  const ownbioContext = useContext(OwnBioContext); 
  const { ownbio } = ownbioContext;

  const petContext = useContext(PetContext);
  const { pet } = petContext; 

  useEffect(() => {
    authContext.loadUser();
    // eslint-disable-next-line
  }, []);

  const createOwnBio = (
    <Fragment>
      <div>
        <CreateOwnBioClient />
      </div>
      <div>
        <OwnBioFilter />
        <OwnBios />
      </div>
    </Fragment>
  );

  const createPet2Bio = (
    <Fragment>
      {/* <div>
        <CreatePet2BioForm />
      </div> */}
      <div>
        <OwnBios />
        {/* <Pets /> */} 
      </div>
    </Fragment>
  ); 
  
  return (
    <Fragment>
      <div className="grid-2">
        {ownbio !== null && ownbio.length !== 0 && !loading ? createPet2Bio : createOwnBio}
      </div>
    </Fragment>
  ); 
}

export default ShowEvents; 