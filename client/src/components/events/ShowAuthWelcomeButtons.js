import React from 'react';
import { Link } from 'react-router-dom';

const ShowAuthWelcomeButtons = () => {
  return (
    <div className="showevents-buttons">
      <Link to="/edit-user" 
        className="btn btn-dark btn-sm">
          {/* <i className=""></i> */}
          Update User
      </Link>
      <Link to="/edit-ownbio" 
        className="btn btn-dark btn-sm">
          <i className="fas fa-user-circle"></i>
          {' '}
          Edit Owner Bio
      </Link>
      <Link to="/delete-ownbio" 
        className="btn btn-dark btn-sm">
          {/* <i className=""></i> */}
          Delete Owner Bio
      </Link>
      <Link to="/edit-pet" 
        className="btn btn-dark btn-sm">
          {/* <i className=""></i> */}
          Update Pet Data
      </Link>
      <Link to="/delete-pet" 
        className="btn btn-dark btn-sm">
          {/* <i className=""></i> */}
          Delete Pet
      </Link>
      <Link to="/delete-petbio" 
        className="btn btn-dark btn-sm">
          {/* <i className=""></i> */}
          Delete Pet Bio
      </Link>
      <Link to="/edit-addy" 
        className="btn btn-dark btn-sm">
          {/* <i className=""></i> */}
          Update Address
      </Link>
      <Link to="/edit-vet" 
        className="btn btn-dark btn-sm">
          {/* <i className=""></i> */}
          Update Vet
      </Link>
      <Link to="/edit-specialneeds" 
        className="btn btn-dark btn-sm">
          {/* <i className=""></i> */}
          Update Special Needs
      </Link>
    </div>
  )
}

export default ShowAuthWelcomeButtons;
