import React from 'react';

export const ShowNotFound = () => {
  return (
    <div>
      <h2>
        <p className="lead">The page you are looking for does not exist. The links above might prove useful instead..</p>
      </h2>
    </div>
  )
};

export default ShowNotFound;