import React, { Component } from 'react';

class SuccessRegClient extends Component {
  render() {
    return (
      <div className="title text-success">
        <div className="register-success">
          <h3>Successfully registered!</h3>
        </div>  
      </div>
    );
  }
}

export default SuccessRegClient; 