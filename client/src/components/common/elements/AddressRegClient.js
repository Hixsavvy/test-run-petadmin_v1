import React, { Component } from 'react'; 

class AddressRegClient extends Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }
   
  handleSubmit(e) {
    e.preventDefault();
    this.props.onSubmit();
   // this.props.addAddress(this.state.address);
  }

  render() {
    return (
      <div className="owner-address">
        <div className="form-container">
          <div className="w3-row-padding">
            <div className="w3-half w3-margin-bottom">
              <form className="w3-container w3-card-4" onSubmit={this.handleSubmit}>
                <p className="subtitle has-text-grey">Please enter address details.</p>

                <div className="w3-section">
                  <input 
                    className="w3-input" 
                    id="addHouseNameInput"
                    type="text" 
                    name="address"
                    key="house" 
                    placeholder="Owners house name/number"
                    value={this.props.address_house}
                    onChange={this.props.onAddressHouseChange}
                  />
                  <label>House name/number:</label>
                </div>
 
                <div className="w3-section">
                  <input 
                    className="w3-input" 
                    id="addStreetName1Input"
                    type="text" 
                    name="address"
                    key="street" 
                    placeholder="Owners street name and number"
                    value={this.props.address_street}
                    onChange={this.props.onAddressStreetChange}
                  />
                  <label>Street name:</label>
                </div>

                <div className="w3-section">
                  <input 
                    className="w3-input" 
                    id="addStreetName2Input"
                    type="text" 
                    name="address"
                    key="street2" 
                    placeholder="Owners street name and number"
                    value={this.props.address_street2}
                    onChange={this.props.onAddressStreet2Change}
                  />
                  <label>Street name:</label>
                </div>

                <div className="w3-section">
                  <input 
                    className="w3-input" 
                    id="addPostCodeInput"
                    type="text" 
                    name="address"
                    key="postcode" 
                    placeholder="Owners postcode"
                    value={this.props.address_post_code}
                    onChange={this.props.onPostCodeChange}
                  />
                  <label>Post code:</label>
                </div>
                
                <div className="w3-section">
                  <input 
                    className="w3-input" 
                    id="addCityInput"
                    type="text" 
                    name="address"
                    key="city" 
                    placeholder="Owners city"
                    value={this.props.address_city}
                    onChange={this.props.onAddressCityChange}
                  />
                  <label>City:</label>
                </div>
                    
                <div className="w3-section">
                  <p>
                  <button 
                    className="w3-button w3-block w3-black w3-margin-bottom"
                    id="AddAddressButton"
                    type="submit">
                      Next
                  </button>
                  </p>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div> 
    );
  }
}
 
export default AddressRegClient;