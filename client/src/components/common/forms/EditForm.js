import React, { Fragment, useState, useContext, useEffect } from 'react';
import AuthContext from '../../../context/auth/authContext'; 
import { Link, withRouter } from 'react-router-dom';

const EditForm = () => {
  const authContext = useContext(AuthContext); 

  const { loadUser, updateUser, clearCurrentUser, clearUser, history } = authContext; 
  
  const [user, setUser] = useState({
    firstname: '',
    lastname: '',
    contactnumber: '',
    email: '',
    //avatar: ''
  });

  useEffect(() => {
    loadUser();
    
    if (lastname || contactnumber || email || avatar || lastname && contactnumber && email && avatar) {
      //if (current !== null) {
      // Set the Form with the current value (obj) 
      // with whatever ownbio we click "edit" for
      setUser({
        lastname: loading || !user.lastname ? '' : user.lastname,
        contactnumber: loading || !user.contactnumber ? '' : user.contactnumber,
        email: loading || !user.email ? '' : user.email,
        avatar: loading || !user.avatar? '' : user.avatar
      }); 
    // } else {
    //   // Clear it to default state
      // setUser({
      //   lastname: '', 
      //   contactnumber: '',
      //   email: '',
      //   avatar: '',
      // });
    }
  }, [loading]); // To avoid error msgs, add these two dependencies
                                // for when ownbioContext, or current, values change

  const { lastname, contactnumber, email, avatar } = user;

  const onChange = e => setUser({ ...user,  [e.target.name]: e.target.value }); 
  
  const onSubmit = async e => {
    e.preventDefault();
    if (current === null) {
     setUser(user); 
    } else {
      updateUser(user);
    }
     clearAll();
  };

  const clearAll = () => {
    clearCurrentUser();
  }
    
  return (
    <Fragment>
      <div className="edit-user">
        <div className="form-container">
          <div className="w3-row-padding">
            <div className="w3-half w3-margin-bottom">
              
              <form className="w3-container w3-card-4" onSubmit={onSubmit}>
                <div className="w3-container w3-border">
                {/* <h3 className="w3-panel w3-black w3-opacity">{current ? "Edit user" : "Add user"}</h3> */}
                <p className="w3-wide w3-center">Edit details below</p>
                </div>
 
               <div className="w3-section">
                  <input 
                      className="w3-input" 
                      id="editLastNameInput"
                      type="text" 
                      name="lastname" 
                      placeholder="edit lastname here"
                      value={lastname}
                      onChange={onChange}
                  />
                </div>

                <div className="w3-section">
                  <input 
                      className="w3-input" 
                      id="editContactNumberInput"
                      type="number" 
                      name="contactnumber" 
                      placeholder="edit phone here"
                      value={contactnumber}
                      onChange={onChange}
                    />
                </div>

                <div className="w3-section">
                  <input 
                    className="w3-input" 
                    id="editEmailInput"
                    type="email" 
                    name="email" 
                    placeholder="edit email here"
                    value={email}
                    onChange={onChange}
                  />
                </div>

                {/* AVATAR HERE?? */}

                <div className="w3-section">
                  <p>
                  <input 
                    className="w3-input w3-block w3-black w3-margin-bottom"
                    id="UserEditButton"
                    type="submit"
                    //value={current ? "Update" : "Add owner bio"}>
                      Update user
                  />
                  </p>
                </div>
                {current && (
                  <div className="w3-section">
                    <button 
                    className="w3-button w3-block w3-light-blue w3-margin-bottom"
                    id="ClearUserButton"
                    type="submit" onClick={clearAll}>
                      Clear
                    </button>
                  </div>
                )}
              </form>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default EditForm;