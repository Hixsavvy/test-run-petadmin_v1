import React, { Fragment } from 'react';
import dawg_walk_owner_spinner from '../img/dawg-walk-owner-spinner.gif';

const Spinner = () =>
  <Fragment>
    <img src={dawg_walk_owner_spinner} alt="Loading..." 
      style={{ width: '100px', margin: 'auto', display: 'block' }} />
  </Fragment>

export default Spinner;