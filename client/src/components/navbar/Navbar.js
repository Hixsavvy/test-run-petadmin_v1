import React, { Fragment, useContext } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'; 
import AuthContext from '../../context/auth/authContext';
import OwnBioContext from '../../context/ownbio/ownbioContext'; 

const Navbar = ({ icon, title }) => {
  const authContext = useContext(AuthContext);
  const ownbioContext = useContext(OwnBioContext);

  const { isAuthenticated, logout, user } = authContext;
  const { clearOwnBio } = ownbioContext;

  const onLogout = () => {
    logout();
    clearOwnBio(); 
  }
  const authLinks = (
    <Fragment>
      <ul className="navbar-nav ml-auto">
        {/* <li>Welcome {' '}{ user && user.firstname }</li> */}
        <i className="fas fa-user"> {' '}Welcome {' '}{ user && user.firstname }</i> 
        <li className="nav-item">
          <Link to="/add-pet" className="nav-link">Add Pet</Link>
        </li> 
        <li className="nav-item">
          <a onClick={onLogout} href="#!">
            <i className="fas fa-sign-out-alt" />{' '}
            <span className="hide-sm">Logout</span>
            {/* <span className="nav-link">Logout</span> */}
          </a>
        </li>
      </ul>
    </Fragment>
  ); 

  const guestLinks = (
    <Fragment>
      <ul className="navbar-nav ml-auto">
      <li className="nav-item">
        <Link to="/register" className="nav-link">Add new client</Link>
      </li>
      <li className="nav-item">
        <Link to="/get-client" className="nav-link">Get client</Link>
      </li>
      </ul>
    </Fragment>
  ); 

  return (
    <Fragment>
        <nav className="navbar navbar-expand-sm bg-primary mb-4">
        {/* <nav className="navbar-left bg-primary"> */} 
          <div className="container">
            {/* <div className="navbar-brand"> */}
              <a className="navbar-brand" href="/get-client">
                <h2>
                  <i className={icon} />
                  {title}
                </h2>
              </a>
              {isAuthenticated ? authLinks : guestLinks}
            {/* </div> */}
          </div>
        </nav>
    </Fragment>
  ); 
}

Navbar.defaultProps = {
  title: "Pet Admin MVP",
  icon: "fas fa-dog fa-fw"
}; 

Navbar.propTypes = {
  title: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired
};

export default Navbar;
