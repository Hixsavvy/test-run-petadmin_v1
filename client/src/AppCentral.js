import React, { Fragment } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Navbar from '../src/components/navbar/Navbar';
//import Categories from '../src/categories/Categories';
import ShowEvents from '../src/components/events/ShowEvents';
import OwnbioState from './context/ownbio/OwnbioState';
import './App.css';
//import Owners from '../src/components/ownbios/owners/Owners';
//import OwnerBio from '../src/components/users/owners/OwnerBio';
//import RegisterUserClient10 from '../src/components/users/auth/RegisterUserClient10';
//import RegUserClientForm from './components/users/auth/RegUserClientForm';
//import SupAdminAddUser from './components/users/auth/SupAdminAddUser';

const AppCentral = () => {

  return (
    <OwnbioState>
      <Router>
        <Fragment>
          <Navbar />
          <div className="container">
              <Switch>
                {/*<SupAdminAddUser />*/}
                {/*<RegUserClientForm />*/}
                {/*<RegisterUserClient10 />*/}
                <Route exact path='/' component={ShowEvents} />
                {/*<Route exact path='/register' component={tba} />*/}
              </Switch>
            </div>
          </Fragment>
      </Router>
    </OwnbioState>
  );
}
export default AppCentral; 
