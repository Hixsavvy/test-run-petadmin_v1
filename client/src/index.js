import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
//import AppCentral from './AppCentral';

ReactDOM.render(<App />, document.getElementById('root'));
//ReactDOM.render(<AppCentral />, document.getElementById('root'));