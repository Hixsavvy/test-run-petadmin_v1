const express = require('express');
const apiRouter = express.Router(); 

const auth = require('../../../middleware/auth'); 
const { check, validationResult } = require('express-validator');

const Pet = require('../../../models/v10/Pet');
const OwnBio = require('../../../models/v10/OwnBio');
const User = require('../../../models/v1/User');


// @route   GET api/v10/pets/:id 
// @desc    Get pet data by id
// @access  Private 
apiRouter.get('/:id', auth, async (req, res) => {
  try {
    // const ownbio = await OwnBio.findOne({ user: req.user.id });
    const pet = await Pet.findOne({user: req.user.id});


    
    // await Pet.findById(req.params.id, req.body, (err, user) => {
    //   if(!pet) {
    //     return res.status(400).json({ msg: 'Pet not found!' });
    //   };  

    //   res.json(pet);
    // })
    await Pet.findById(req.params.id)
      //.populate('pet', ['age','petbio']);

    // const pet = await OwnBio.findOne({ownbio: req.ownbio.id})
    //   .populate('pet', ['age','petbio']);

    if(!pet) {
      return res.status(400).json({ msg: 'Pet not found!' });
    }

    res.json(pet);
    
  } catch (err) {
    console.error(err.message);

    // To prevent status(500) error down below running prematurely when 
    // non-formatted ObjectId-type possibly mis-spell or malicious (input) 
    // checking, we use the status(400) error handler as below 
    if(err.kind === 'ObjectId') {
      return res.status(400).json({ msg: 'Pet not found - 2 !!' });
    }

    res.status(500).send('Server error, something went wrong!');
  }
});



                      // @route   GET api/v10/pet/get-pet 
                      // @desc    Get current owner's pet data - not sure its needed
                      // @access  Private 
                      apiRouter.get('/get-pet', auth, async (req, res) => {

                        try {
                          let ownbio = await OwnBio.findOne({ user: req.user.id }); 

                          if (!ownbio || ownbio === null) {
                            return res.status(400).json({msg: 'No bio for this owner !'});
                          }

                            // Redirect to owner bio form here 

                          // var id = req.params.id;

                          // let pet = await Pet.find({_id: new mongodb.ObjectId(id)});

                          let pet = await Pet.findOne({ user: req.user.id });

                          await Pet.findById(req.params.id, req.body, (err, user) => {
                            if(!pet) {
                              return res.status(400).json({ msg: 'Pet not found!' });
                            };  

                            res.json(pet);
                          })

                          //    .populate('pet', ['age','petbio']);

                         // const pet = await OwnBio.findOne({ownbio: req.ownbio.id})
                            //.populate('pet', ['age','petbio']);
                          //  .populate('pet');

                          // const pet = await 
                          //   Pet
                          //     .findOne({ pets: req.pet.id })
                          //     //.populate('user', ['_id', 'contactnumber']);// Pull required data from user profile 

                              //res.json(pet); 
                              // if(!pet) {
                              //   return res.status(400).json({ msg: 'Pet not found!' });
                              // }
                          
                              // res.json(pet);
                            
                                    
                        } catch (err) {
                          console.error(err.message);
                          res.status(500).send('Server error, something went wrong!');
                        }
                      });




// @route   POST api/v10/pets/add-pet
// @desc    Register pet route
// @access   Private
apiRouter.post('/add-pet', 
[
  auth, 
  [
    /*
    check('firsteverarrivaldate', 'When was the first ever date your pet arrived here')
      .not()
      .isEmpty(),
      */
    check('age', 'How old is your pet please?')
      .not()
      .isEmpty()
  ]
], 
async (req, res) => {
  const errors = validationResult(req);
  if(!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  const {
    firsteverarrivaldate,
    age
  } = req.body;

// Build pet snapshot object
  const petFields = {};

  petFields.user = req.user.id;
  //petFields.ownbio = req.params.id;

  if(age) petFields.age = age;
  if(firsteverarrivaldate) petFields.firsteverarrivaldate = firsteverarrivaldate;


  try {

    let ownbio = await OwnBio.findOne({ user: req.user.id });

      if (!ownbio) {
        return res.status(400)
          .json({ msg: 'No bio for this owner!' }); 
      }

        // Create pet 
        const pet = new Pet(petFields);

        await pet.save();

        res.json(pet);

        // Update ownbio 
        // ownbio = await OwnBio.findOneAndUpdate(
        //    { user: req.user.id },
        // //   { ownbio: req.ownbio.id }, 
        //    { $set: petFields, pet },
        //    { new: true },
        //    { createdAt: true }
        //  );

     // }
            
      // ownbio.pet.unshift(petFields);

    //     res.json(pet); 

      

  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server error, something went wrong!');
  }

});



        // @route   POST api/v10/pets/link-pet-to-ownbio // POST request used, rather than a  
          // PUT although we are updating data in an existing collection - personal preference
          // @desc    Add petbio to pet data 
          // @access  Private
          //apiRouter.put('/put-link-pet-to-ownbio',
          apiRouter.post('/link-pet-to-ownbio', 
          [
            auth, 
            [
              check('petname', 'Please enter pet name')
                .not()
                .isEmpty(),
              check('pettype', 'Please enter pet type')
                .not()
                .isEmpty(),
              check('petbreed', 'Please enter pet breed')
                .not()
                .isEmpty()
            ]
          ], 
          async (req, res) => {
            const errors = validationResult(req);
            if(!errors.isEmpty()) {
              return res.status(400).json({ errors: errors.array() });
            }

            const {
              petname,
              pettype,
              petbreed
            } = req.body;

            const petBioFields = {
              petname,
              pettype,
              petbreed
            };

            try {
              // Fetch pet to add pet bio 
              const pet = await Pet.findOne({ user: req.user.id });

              // What if user has no bio?
              if(!pet) {
                return res.status(400)
                  .json({ msg: 'No pet for this owner!' }); 

               }  
              
                // Where petbio already exists? Unshift should deal with that

                // Push petbio array onto the pet data using unshift (not PUSH) so it goes
                // into the beginning rather than at the end so we get the most recent first 
                pet.petbio.unshift(petBioFields);

                await pet.save();

                // Update owner bio - where it already exists 
                let ownbio = await OwnBio.findOneAndUpdate(
                  { user: req.user.id }, 
                  { $set: petBioFields, pet },
                  { new: true }
                );

                // Push pet array onto the owner bio using unshift (not PUSH) so it goes
                // into the beginning rather than at the end so we get the most recent first 
                //ownbio.pet.unshift(pet);

                await ownbio.save();

                res.json(ownbio);

            } catch (err) {
              console.error(err.message);
              res.status(500).send('Server error, something went wrong!');
            }

          });



          // @route   POST api/v10/petsownbio/link-pet-2-ownbio // POST request used, rather than a  
          // PUT although we are updating data in an existing collection - personal preference
          // @desc    Add petbio to pet data 
          // @access  Private
          //apiRouter.put('/put-link-pet-2-ownbio',
          apiRouter.post('/link-pet-2-ownbio', 
          [
            auth, 
            [
              check('petname', 'Please enter pet name')
                .not()
                .isEmpty(),
              check('pettype', 'Please enter pet type')
                .not()
                .isEmpty(),
              check('petbreed', 'Please enter pet breed')
                .not()
                .isEmpty()
            ]
          ], 
          async (req, res) => {
            const errors = validationResult(req);
            if(!errors.isEmpty()) {
              return res.status(400).json({ errors: errors.array() });
            }

            const {
              petname,
              pettype,
              petbreed
            } = req.body;

            const petBioFields = {
              petname,
              pettype,
              petbreed
            };

            try {
              const ownbio = await OwnBio.findOne({user: req.user.id});
              // Fetch pet to add pet bio 
              const pet = await Pet.findOneAndUpdate({ user: req.user.id });

              // What if user has no bio?
              if(!pet) {
                return res.status(400)
                  .json({ msg: 'No pet for this owner!' }); 

               }  
              
                // Where petbio already exists? Unshift should deal with that

                // Push petbio array onto the pet data using unshift (not PUSH) so it goes
                // into the beginning rather than at the end so we get the most recent first 
                 pet.petbio.unshift(petBioFields);


                // Update pet 
                // await Pet.updateOne(
                //    { user: req.user.id },
                // //   { pet: req.pet.id }, 
                //    { $set: petBioFields },
                //    { new: true },
                //    { $currentDate: true }
                //  );


                await pet.save();

                // Update owner bio - where it already exists 
                //let ownbio = await OwnBio.findOneAndUpdate(
                ownbio = await OwnBio.updateOne(
                  { user: req.user.id }, 
                  { $set: petBioFields, pet },
                  { new: true }
                );

                // Push pet array onto the owner bio using unshift (not PUSH) so it goes
                // into the beginning rather than at the end so we get the most recent first 
                //ownbio.pet.unshift(pet);

                await ownbio.save();

                res.json(ownbio);
                //res.json(pet);

            } catch (err) {
              console.error(err.message);
              res.status(500).send('Server error, something went wrong!');
            }

          });



          // @route   POST api/v10/pets/add-a-pet // POST request used, rather than a  
          // PUT although we are updating data in an existing collection - personal preference
          // @desc    Add pet data with petbio
          // @access  Private
          //apiRouter.put('/put-link-pet-ownbio',
          //apiRouter.post('/link-pet-ownbio',
          apiRouter.post('/add-a-pet', 
          [
            auth, 
            [
              check('age', 'How old is your pet please?')
                .not()
                .isEmpty(),
              check('petname', 'Please enter pet name')
                .not()
                .isEmpty(),
              check('pettype', 'Please enter pet type')
                .not()
                .isEmpty(),
              check('petbreed', 'Please enter pet breed')
                .not()
                .isEmpty()
            ]
          ], 
          async (req, res) => {
            const errors = validationResult(req);
            if(!errors.isEmpty()) {
              return res.status(400).json({ errors: errors.array() });
            }

            const {
              age,
              petname,
              pettype,
              petbreed,
              firsteverarrivaldate,
              isBoarder
            } = req.body;

            const petBioFields = {
              age,
              petname,
              pettype,
              petbreed,
              firsteverarrivaldate,
              isBoarder
            };

            petBioFields.user = req.user.id;

            try {

              let ownbio = await OwnBio.findOne({ user: req.user.id });

              if (!ownbio) {
                return res.status(400)
                  .json({ msg: 'No bio for this owner!' }); 
              }

              let pet = await Pet.findOne({user: req.user.id});

              if (!pet) {

                // Create pet 
                pet = new Pet(petBioFields);

                // Push petbio array so its nested inside pet
                await pet.petbio.unshift(petBioFields);
                await pet.petbio.push(petBioFields);

                await pet.save();
              }

              else if (pet) {

                // Push petbio array so its nested inside pet
                await pet.petbio.unshift(petBioFields);

                await pet.save();

              }

                // Update owner bio - where it already exists  
                ownbio = await OwnBio.updateOne(
                  { user: req.user.id }, 
                  { $set: petBioFields, pet },
                  { new: true }
                );

                  res.json(ownbio);
              
            } catch (err) {
              console.error(err.message);
              res.status(500).send('Server error, something went wrong!');
            }

          });


          // @route   POST api/v10/pets/link-pet-ownbio // POST request used, rather than a  
          // PUT although we are updating data in an existing collection - personal preference
          // @desc    Add pet data with petbio
          // @access  Private
          //apiRouter.put('/put-link-pet-ownbio',
          apiRouter.post('/link-pet-ownbio',
          [
            auth, 
            [
              check('status', 'Daily or boarding?')
                .not()
                .isEmpty()
            ]
          ], 
          async (req, res) => {
            const errors = validationResult(req);
            if(!errors.isEmpty()) {
              return res.status(400).json({ errors: errors.array() });
            }
          
            const {
              status,
              isBoarder,
              firsteverarrivaldate
            } = req.body;
          
          // Build pet snapshot object
            const petFields = {};
          
            petFields.user = req.user.id;
            //petFields.ownbio = req.params.id;
          
            if(status) petFields.status = status;
            if(isBoarder) petFields.isBoarder = isBoarder;
            if(firsteverarrivaldate) petFields.firsteverarrivaldate = firsteverarrivaldate;

          try {
              const ownbio = await OwnBio.findOne({user: req.user.id});
              // Fetch pet to add pet bio 
              const pet = await Pet.findOneAndUpdate({ user: req.user.id });

              // What if user has no bio?
              if(!pet) {
                return res.status(400)
                  .json({ msg: 'No pet for this owner!' }); 

               }  
              
                // Where petbio already exists? Unshift should deal with that

                // Push petbio array onto the pet data using unshift (not PUSH) so it goes
                // into the beginning rather than at the end so we get the most recent first 
                 pet.petbio.unshift(petFields);


                // Update pet 
                // await Pet.updateOne(
                //    { user: req.user.id },
                // //   { pet: req.pet.id }, 
                //    { $set: petBioFields },
                //    { new: true },
                //    { $currentDate: true }
                //  );


                await pet.save();

                // Update owner bio - where it already exists 
                //let ownbio = await OwnBio.findOneAndUpdate(
                ownbio = await OwnBio.updateOne(
                  { user: req.user.id }, 
                  { $set: petFields, pet },
                  { new: true }
                );

                // Push pet array onto the owner bio using unshift (not PUSH) so it goes
                // into the beginning rather than at the end so we get the most recent first 
                //ownbio.pet.unshift(pet);

                await ownbio.save();

                res.json(ownbio);
                //res.json(pet);

            } catch (err) {
              console.error(err.message);
              res.status(500).send('Server error, something went wrong!');
            }

          });



        // @route   POST api/v10/petsownbio/add-petbio // POST request used, rather than a  
          // PUT although we are updating data in an existing collection - personal preference
          // @desc    Add petbio to pet data 
          // @access  Private 
          apiRouter.post('/add-petbio', 
          [
            auth, 
            [
              check('petname', 'Please enter pet name')
                .not()
                .isEmpty(),
              check('pettype', 'Please enter pet type')
                .not()
                .isEmpty(),
              check('petbreed', 'Please enter pet breed')
                .not()
                .isEmpty()
            ]
          ], 
          async (req, res) => {
            const errors = validationResult(req);
            if(!errors.isEmpty()) {
              return res.status(400).json({ errors: errors.array() });
            }

            const {
              petname,
              pettype,
              petbreed
            } = req.body;

            const petBioFields = {
              petname,
              pettype,
              petbreed
            };

            try {
              // Fetch pet to add pet bio 
              const pet = await Pet.findOne({ user: req.user.id });

              // What if user has no bio?
              if(!pet) {
                return res.status(400)
                  .json({ msg: 'No pet for this owner!' }); 
              }

              // Where petbio already exists? Unshift should deal with that

              // Push petbio array onto the pet data using unshift (not PUSH) so it goes
              // into the beginning rather than at the end so we get the most recent first 
              pet.petbio.unshift(petBioFields);

              await pet.save();

              res.json(pet);

            } catch (err) {
              console.error(err.message);
              res.status(500).send('Server error, something went wrong!');
            }

          });


          



          // @route   POST api/v10/petsownbio/age-petbio // POST request used, rather than a  
          // PUT although we are updating data in an existing collection - personal preference
          // @desc    Add petbio to pet data 
          // @access  Private 
          apiRouter.post('/age-petbio', 
          [
            auth, 
            [
              // check('age', 'Please enter pet age')
              //   .not()
              //   .isEmpty(),
              check('petname', 'Please enter pet name')
                .not()
                .isEmpty(),
              check('pettype', 'Please enter pet type')
                .not()
                .isEmpty(),
              check('petbreed', 'Please enter pet breed')
                .not()
                .isEmpty()
            ]
          ], 
          async (req, res) => {
            const errors = validationResult(req);
            if(!errors.isEmpty()) {
              return res.status(400).json({ errors: errors.array() });
            }

            const {
              age,
              petname,
              pettype,
              petbreed
            } = req.body;

            const newPet = new Pet({ age });

            const petBioFields = {
              petname,
              pettype,
              petbreed
            };

            if(age) age = age;

            try {
              // ====
              //const ownbio = await OwnBio.findOne({ user: req.user.id });
              let ownbio = await OwnBio.findOne({ user: req.user.id });

                // What if user has no bio?
                if(!ownbio) {
                  return res.status(400)
                    .json({ msg: 'No owner bio for this user!' }); 

                } else if (ownbio) {
                
                // Save new pet
                //const pet = await newPet.save();
                let pet = await newPet.save();

                // Update owner bio - where it already exists 
                ownbio = await OwnBio.findOneAndUpdate(
                  { user: req.user.id }, 
                  //{ $set: petBioFields, pets },
                  { $set: petBioFields, pet },
                  { new: true }
                );

                return res.json(ownbio);

              } else {

//-------------------------

              // Fetch pet to add pet bio 
              //const pet = await Pet.findOne({ user: req.user.id });
              //await Pet.findOne({ user: req.user.id });
              //let pet;

                // Create pet bio fields - where it does not already exist
                pet = new Pet(petBioFields, age);

                await Pet.findOne({ user: req.user.id });

                    // Push petbio array onto the pet data using unshift (not PUSH) so it goes
                    // into the beginning rather than at the end so we get the most recent first 
                    pet.petbio.unshift(petBioFields);

                    await pet.save();

                    res.json(pet);
              }
                //-----------------
              //}
              // let ownbio = await OwnBio.findOne({ user: req.user.id });

                // Push pet array onto the owner bio using unshift (not PUSH) so it goes
                // into the beginning rather than at the end so we get the most recent first 
                //ownbio.pets.unshift(newPet);
                ownbio.pet.unshift(newPet);

                await ownbio.save();

                res.json(ownbio);

              // ===

               

            } catch (err) {
              console.error(err.message);
              res.status(500).send('Server error, something went wrong!');
            }

          });



                                // @route   POST api/v10/pets/add_pet - unfinished
                                // @desc    Register pet route
                                // @access   Private
                                apiRouter.post('/add_pet', 
                                [
                                  auth, 
                                  [
                                    /*
                                    check('firsteverarrivaldate', 'When was the first ever date your pet arrived here')
                                      .not()
                                      .isEmpty(),
                                      */
                                    check('age', 'How old is your pet please?')
                                      .not()
                                      .isEmpty()
                                  ]
                                ], 
                                async (req, res) => {
                                  const errors = validationResult(req);
                                  if(!errors.isEmpty()) {
                                    return res.status(400).json({ errors: errors.array() });
                                  }

                                  const {
                                    firsteverarrivaldate,
                                    age
                                  } = req.body;

                                // Build pet snapshot object
                                  const petFields = {};

                                  petFields.user = req.user.id;
                                  //petFields.ownbio = req.params.id;

                                  if(age) petFields.age = age;
                                  if(firsteverarrivaldate) petFields.firsteverarrivaldate = firsteverarrivaldate;


                                  try {

                                    let ownbio = await OwnBio.findOne({ user: req.user.id });

                                      if(ownbio) {
                                        
                                        // Create pet
                                        const pet = new Pet(petFields);

                                        await pet.save();

                                        return res.json(pet);

                                      } else if (ownbio && pet) {
                                  
                                        // Update ownbio 
                                        ownbio = await OwnBio.findOneAndUpdate(
                                          { user: req.user.id },
                                      //   { ownbio: req.ownbio.id }, 
                                          { $set: petFields, pet },
                                          { new: true },
                                          { createdAt: true }
                                        );

                                            res.json(ownbio);

                                      }
                                            
                                    //   ownbio.pet.unshift(petFields);

                                     

                                      

                                  } catch (err) {
                                    console.error(err.message);
                                    res.status(500).send('Server error, something went wrong!');
                                  }

                                });



// @route   DELETE api/v10/pets/:id
// @desc    Delete pet data 
// @access  Private
apiRouter.get('/:id', auth, async (req, res) => {
  try {
    const pet = await Pet.findById(req.params.id);

    if(!pet) {
      return res.status(400).json({ msg: 'Pet not found!' });
    }

    // Check user to confirm its the right pet owner.
    // But logged in user (req.user.id) is a string, and pet.user is
    // and ObjectId (not a string), use toString to prevent conflict errors
    if(pet.user.toString !== req.user.id) {
      return res.status(401).json({ msg: 'User not authorised!' })
    }

    await pet.remove();

    res.json({ msg: 'Pet removed' });
    
  } catch (err) {
    console.error(err.message);

    // To prevent status(500) error down below running prematurely when
    // non-formatted ObjectId-type possibly mis-spell or malicious (input) 
    // checking, we use the status(400) error handler as below
    if(err.kind === 'ObjectId') {
      return res.status(400).json({ msg: 'Pet not found!' });
    }

    res.status(500).send('Server error, something went wrong!');
  }
});



// @route   GET api/v10/pets/all
// @desc    Get all pets data 
// @access  Private
apiRouter.get('/all', auth, async (req, res) => {
  try {
    const pets = await Pet.find().sort({ date: -1 });

    res.json(pets);
    
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server error, something went wrong!');
  }
});





    
            
            
            
                                // @route   POST api/v10/pets/add-pet-bio
                                // @desc    Add pet bio to pet data
                                // @access  Private 
                                // apiRouter.post('/add-pet-bio', 
                                // [
                                //   auth, 
                                //   [
                                //     check('petname', 'Please enter pet name')
                                //       .not()
                                //       .isEmpty(),
                                //     check('pettype', 'Please enter pet type')
                                //       .not()
                                //       .isEmpty(),
                                //     check('petbreed', 'Please enter pet breed')
                                //       .not()
                                //       .isEmpty()
                                //   ]
                                // ], 
                                // async (req, res) => {
                                //   const errors = validationResult(req);
                                //   if(!errors.isEmpty()) {
                                //     return res.status(400).json({ errors: errors.array() });
                                //   }

                                  

                                //     const {
                                //       petname,
                                //       pettype,
                                //       petbreed
                                //     } = req.body;
                                    
                                //     const petbio = {
                                //       petname,
                                //       pettype,
                                //       petbreed,
                                //       //ownbio: req.ownbio.id
                                //     }

                                //     // const newPetbio = new Petbio({
                                //     //   petname,
                                //     //   pettype,
                                //     //   petbreed,
                                //     //   ownbio: req.ownbio.id
                                //     // });

                                //     // Build petbio object
                                //     const petbioFields = {};

                                //     // petbioFields.ownbio = req.ownbio.id;
                                    
                                //     if(petname) petbioFields.petname = petname;
                                //     if(pettype) petbioFields.pettype = pettype;
                                //     if(petbreed) petbioFields.petbreed = petbreed;
                                    
                                //   try {
                                //     let ownbio = await OwnBio.findOne({ user: req.user.id });

                                //     // What if user has no bio?
                                //     if(!ownbio) {
                                //       return res.status(400)
                                //         .json({ msg: 'No owner bio for this user!' }); 
                                //     }

                                //     let pet = Pet.findOne({ user: req.user.id });

                                //     if (pet) {
                                //       // Save new petbio
                                //       pet = await pet.save();

                                //       //let pet = await Pet.findOneAndUpdate(
                                //       await Pet.findOneAndUpdate(
                                //         { ownbio: req.params.id },
                                //         { $set: { 
                                //           "pets.$[]" : petbio 
                                //         } 
                                //         },
                                //         { new: true }
                                //       )
                                      
                                //       // Push petbio into pets array of the owner bio using unshift (not PUSH) so it goes
                                //       // into the beginning rather than at the end so we get the most recent first 
                                //       //pets.pet.unshift(petbio);
                                //       //ownbio.pets.unshift(petbioFields);

                                      
                                      
                                //       //await petbio.save();

                                //       // Update petbio data where it exists
                                //       //   await Pet.findOneAndUpdate(
                                //       //   { user: req.user.id },
                                //       //   //{ ownbio: req.ownbio.id },
                                //       //   //{ pets: req.params.id },
                                //       //   // { pet: req.params.id },
                                //       //   { $set: 
                                //       //     { 
                                //       //       petbioFields, pets, petbio
                                //       //     }  
                                //       //   },
                                //       //   { new: true }
                                //       // );
                                //     }

                                //     console.log(ownbio);
                                //       //return res.json(ownbio);
                                //       //return res.json(petbio);


                                //       // Create petbio - where it does not already exist
                                //       //pet = petbio(petbioFields);
                                //       // pet = new petbio(petbioFields);
                                      

                                      

                                //       // await pet.save();

                                //       // await ownbio.save();

                                //       // res.json(ownbio);

                                //   } catch (err) {
                                //     console.error(err.message);
                                //     res.status(500).send('Server error, something went wrong!');
                                //   }

                                // });



                                      // // @route   POST api/v10/pets/addpet-bio
                                      // // @desc    Add pet bio to pet data
                                      // // @access  Private 
                                      // apiRouter.post('/addpet-bio', 
                                      // [
                                      //   auth, 
                                      //   [
                                      //     check('petname', 'Please enter pet name')
                                      //       .not()
                                      //       .isEmpty(),
                                      //     check('pettype', 'Please enter pet type')
                                      //       .not()
                                      //       .isEmpty(),
                                      //     check('petbreed', 'Please enter pet breed')
                                      //       .not()
                                      //       .isEmpty()
                                      //   ]
                                      // ], 
                                      // async (req, res) => {
                                      //   const errors = validationResult(req);
                                      //   if(!errors.isEmpty()) {
                                      //     return res.status(400).json({ errors: errors.array() });
                                      //   }

                                        

                                      //     const {
                                      //       petname,
                                      //       pettype,
                                      //       petbreed
                                      //     } = req.body;

                                      //     const petbio = {
                                      //       petname,
                                      //       pettype,
                                      //       petbreed,
                                      //       pet: req.pets.id
                                      //     };

                                      //   try {
                                      //     let pet = Pet.findOne({ ownbio: req.ownbio.id })

                                      //     // Fetch pet object to add pet bio data 
                                      //     await Pet.findOneAndUpdate(
                                      //       { pet: req.params.id },
                                      //       { $addToSet: 
                                      //         { 
                                      //           petbio 
                                      //         }  
                                      //       },
                                      //       { new: true }
                                      //     ); 

                                      //     res.json(pet);

                                      //   } catch (err) {
                                      //     console.error(err.message);
                                      //     res.status(500).send('Server error, something went wrong!');
                                      //   }

                                      // });


module.exports = apiRouter;