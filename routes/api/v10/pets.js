const express = require('express');
const apiRouter = express.Router(); 

const auth = require('../../../middleware/auth'); 
const { check, validationResult } = require('express-validator');

const Pet = require('../../../models/v10/Pet');
const OwnBio = require('../../../models/v10/OwnBio');
const User = require('../../../models/v1/User');


// @route   GET api/v10/pet/get-pet-doc 
// @desc    Get current owner's pet data - not sure its needed
// @access  Private 
apiRouter.get('/get-pet-doc', auth, async (req, res) => {

  try {
    let pet = await Pet.findOne({ user: req.user.id }); 

    if (!pet) {
      return res.status(400)
          .json({ msg: 'No pet for this owner!' }); 
      }
    
    else if (pet) {
      await Pet.find({});
        //.populate('pet', ['status', 'petbio']); 

        res.json(pet);
    }
    
    // Redirect to pet registration page here (no pet)
          
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server error, something went wrong!');
  }
});


// @route   POST api/v10/pets/add-a-pet // POST request used, rather than a  
// @desc    Add pet data with petbio
// @access  Private
apiRouter.post('/add-a-pet', 
[
  auth, 
  [
    check('age', 'How old is your pet please?')
      .not()
      .isEmpty(),
    check('petname', 'Please enter pet name')
      .not()
      .isEmpty(),
    check('pettype', 'Please enter pet type')
      .not()
      .isEmpty(),
    check('petbreed', 'Please enter pet breed')
      .not()
      .isEmpty(),
    check('specialneeds', 
      'Briefly provide any special needs info for your pet, if any please')
      .not()
      .isEmpty()
  ]
], 
async (req, res) => {
  const errors = validationResult(req);
  if(!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  const {
    age,
    petname,
    pettype,
    petbreed,
    specialneeds,
    firsteverarrivaldate,
    isBoarder
  } = req.body;

  const ownbio = req.body.id;

  const petBioFields = {
    age,
    petname,
    pettype,
    petbreed,
    specialneeds,
    firsteverarrivaldate,
    isBoarder,
    ownbio
  };

  petBioFields.user = req.user.id;

  if(specialneeds) {
    petBioFields.specialneeds = specialneeds
      .split(',')
      .map(specialneed => specialneed.trim());
  };

  try {

    let ownbio = await OwnBio.findOne({ user: req.user.id });

    if (!ownbio) {
      return res.status(400)
        .json({ msg: 'No bio for this owner!' }); 
    }

    let pet = await Pet.findOne({user: req.user.id}); 

    if (!pet) {

      // Create pet 
      pet = new Pet(petBioFields);

      // Push petbio array so its nested inside pet
      await pet.petbio.push(petBioFields);
      //await pet.petbio.push(petBioFields, ownbio.id);
      //await pet.petbio.push(petBioFields, ownbio._id);

      await pet.save();

      //res.json(pet); 
    }

    else if (pet) {
      // Push petbio array so its nested inside pet 
      // Use unshift so newly created pet goes to top of array
      await pet.petbio.unshift(petBioFields);

      await pet.save();

      //res.json(pet); 

    }

      // Update owner bio - where it already exists  
      //ownbio = await OwnBio.updateOne(
      ownbio = await OwnBio.findOneAndUpdate(
        { user: req.user.id }, 
        { $set: petBioFields, pet },
        { new: true }
      );

        res.json(ownbio);
    
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server error, something went wrong!');
  }

});


// @route   GET api/v10/pet/get-petbio  
// @desc    Get current owner's pet data - not sure its needed 
// @access  Private 
apiRouter.get('/get-petbio', auth, async (req, res) => {

  try {
    let pet = await Pet.findOne({ user: req.user.id });
    
    if (!pet) {
      return res.status(400)
          .json({ msg: 'No pet for this owner!' }); 
      }

      else if (pet) {
       await pet.populate({
         path: 'petbio'
       });
       
       res.json(pet.petbio);

      }

  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server error, something went wrong!');
  }
});



// @route   DELETE api/v10/pets/delete 
// @desc    Delete pet [(and ToDo) delete pets data ] 
// @access  Private 
apiRouter.delete('/delete', auth, async (req, res) => {
  try {
   
    await Pet.findOneAndRemove({ user: req.user.id }); 

    // let ownbio = await OwnBio.update({ user: req.user.id }); 

    //   //await ownbio.remove({ 'ownbio': { pet } });
    //   await ownbio.remove(pet);

      // { user: req.user.id },
      // { 'pet': { '$in': ownbio } }
      // {'pet': {$exists: true}}, 
      // {$unset: {pet:1}},
      // false,
    //   true // Callback must be a function - got "true" (so failed in current form) 
    //   // // maybe add .exec() instead of "true" ?
    // );// <----  HERE ?
    //).exec(); // Like this ?

    // let ownbio = await OwnBio.update(
    //   { user: req.user.id },
    //   ownbio.remove(pet)
    //   //{'pet': {$exists: true}}, 
    //   //{$unset: {pet:1}},
    //   //false
    // ); 
    //).exec();

    //let ownbio = await OwnBio.findOne({ user: req.user.id });
    //let ownbio = await OwnBio.findOne({ user: req.user.id });
    //await updateOne({'pet': {$exists: true}}, {$unset: {'pet': 1}}, false, true);

    //await ownbio.remove({ 'ownbio': { pet } });
    //await remove({ 'ownbio': { pet } });
    //ownbio.remove({pet});
    //ownbio.remove(ownbio.pet);
    //await ownbio.remove(ownbio.pet);
    
    //await OwnBio.updateOne(
    //await OwnBio.findOneAndRemove(
    //await OwnBio.remove(
      //{ user: req.user.id },
      //{ pet: req.params.id },
      //{ pet: req.pet.id },
      //{ ownbio: { pet } }
      //{ $pull: {pet} }, 
      //{ $pull: { ownbio: { _id: pet.id } } },
      //{ $pull: { ownbio: { pet: "ObjectId" } } },
      //{ $unset: { pet } }
    //); 

    return res.status(200).send('Pet deleted!'); 
    
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server error, something went wrong!');
  }
});


          { /* currently not in use!! */ }

          // // @route   DELETE api/v10/pets/:id
          // // @desc    Delete pet data 
          // // @access  Private
          // apiRouter.get('/:id', auth, async (req, res) => {
          //   try {
          //     const pet = await Pet.findById(req.params.id);

          //     if(!pet) {
          //       return res.status(400).json({ msg: 'Pet not found!' });
          //     }

          //     // Check user to confirm its the right pet owner.
          //     // But logged in user (req.user.id) is a string, and pet.user is
          //     // and ObjectId (not a string), use toString to prevent conflict errors
          //     if(pet.user.toString !== req.user.id) {
          //       return res.status(401).json({ msg: 'User not authorised!' })
          //     }

          //     await pet.remove();

          //     res.json({ msg: 'Pet removed' });
              
          //   } catch (err) {
          //     console.error(err.message);

          //     // To prevent status(500) error down below running prematurely when
          //     // non-formatted ObjectId-type possibly mis-spell or malicious (input) 
          //     // checking, we use the status(400) error handler as below
          //     if(err.kind === 'ObjectId') {
          //       return res.status(400).json({ msg: 'Pet not found!' });
          //     }

          //     res.status(500).send('Server error, something went wrong!');
          //   }
          // });



// @route   GET api/v10/pets/all
// @desc    Get all pets data 
// @access  Private
apiRouter.get('/all', auth, async (req, res) => {
  try {
    const pets = await Pet.find().sort({ date: -1 });

    res.json(pets);
    
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server error, something went wrong!');
  }
});




                      // @route   GET api/v10/pets/:id 
                      // @desc    Get pet data by id
                      // @access  Private 
                      // apiRouter.get('/:id', auth, async (req, res) => {
                      //   try {
                      //     // const ownbio = await OwnBio.findOne({ user: req.user.id });

                      //     // if (ownbio) {
                      //       // const pet = await Pet.findOne({user: req.user.id});
                      //       // const pet = await Pet.findById(req.params.id, req.body, (err, user) => {
                      //         // if(!pet) {
                      //           // return res.status(400).json({ msg: 'Pet not found!' });
                      //         // }
                      //       // }
                      //     // }
                      //     const pet = await Pet.findOne({user: req.user.id});


                          
                      //     // await Pet.findById(req.params.id, req.body, (err, user) => {
                      //     //   if(!pet) {
                      //     //     return res.status(400).json({ msg: 'Pet not found!' });
                      //     //   };  

                      //     //   res.json(pet);
                      //     // })
                      //     await Pet.findById(req.params.id)
                      //       //.populate('pet', ['age','petbio']);

                      //     // const pet = await OwnBio.findOne({ownbio: req.ownbio.id})
                      //     //   .populate('pet', ['age','petbio']);

                      //     if(!pet) {
                      //       return res.status(400).json({ msg: 'Pet not found!' });
                      //     }

                      //     res.json(pet);
                          
                      //   } catch (err) {
                      //     console.error(err.message);

                      //     // To prevent status(500) error down below running prematurely when 
                      //     // non-formatted ObjectId-type possibly mis-spell or malicious (input) 
                      //     // checking, we use the status(400) error handler as below 
                      //     if(err.kind === 'ObjectId') {
                      //       return res.status(400).json({ msg: 'Pet not found - 2 !!' });
                      //     }

                      //     res.status(500).send('Server error, something went wrong!');
                      //   }
                      // });


module.exports = apiRouter; 